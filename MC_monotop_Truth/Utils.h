#include <math.h>
#include <string>
#include <sstream>
#include <iostream>
#include <utility>

using namespace std;

namespace Utils {
    
  //=====================================================================
  // FindSubstringFromString()
  //=====================================================================
  bool FindSubstringFromString(string str, string substr) {
    size_t start_pos = str.find(substr);
    if(start_pos == string::npos) { return false; }
    return true;
  }
    
  //=====================================================================
  // ToFloat()
  //=====================================================================
  float ToFloat(const string& str) {
    //convert string to float
    istringstream ss(str);
    float ret;
    ss >> ret;
    return ret;
  }

  //=====================================================================
  // ToString()
  //=====================================================================
  string ToString(double number) {
    stringstream ss;
    ss << std::fixed << std::setprecision(1) << number;
    return ss.str();
  }

  //=====================================================================
  // SplitString()
  //=====================================================================
  vector<string> SplitString(string str, const char* delimeter, bool considerEmptyItems) {
    vector<string> vect;
    vect.clear();
      
    for(size_t p=0, q=0; p!=str.npos; p=q) {
      string line = str.substr(p+(p!=0), (q=str.find(delimeter, p+1))-p-(p!=0));

      // remove white spaces at the begining of the string
      int i = 0;
      while (line[i] == ' ') { line.erase(i, 1); i++; }
	
      // check empty items
      i = 0;
      while (line[i] == ',') {
	if (considerEmptyItems) { vect.push_back(""); }
	line.erase(i, 1);
	i++;
      }
	
      if (line.size()>0) { vect.push_back(line); }
    }
    return vect;
  }

}

