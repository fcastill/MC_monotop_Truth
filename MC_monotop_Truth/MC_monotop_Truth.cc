// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Cuts.hh"


#include "Rivet/Projections/ChargedLeptons.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/PromptFinalState.hh"

#include "Rivet/Projections/UnstableFinalState.hh"
// #include "Rivet/Projections/TauFinder.hh"



#include "Rivet/Tools/Logging.hh"
#include "Rivet/Math/LorentzTrans.hh"

// Vetoed Jet inputs
#include "Rivet/Projections/VetoedFinalState.hh"

// event record specific
#include "Rivet/Particle.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"

#include "HepMC/GenEvent.h"

#include "Utils.h"

#include <math.h>
#include <string>
#include <sstream>
#include <iostream>
#include <utility>
#include <bitset>

using namespace std;

namespace Rivet {

  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McProductionCommonParametersMC15
  static const float mass_e = 0.00051 * GeV;
  static const float mass_mu = 0.1057 * GeV;
  static const float mass_tau = 1.777 * GeV;
  static const float mass_d = 0.320 * GeV;
  static const float mass_u = 0.320 * GeV;
  static const float mass_s = 0.5 * GeV;
  static const float mass_c = 1.55 * GeV;
  static const float mass_b = 4.95 * GeV;
  static const float mass_t = 172.5 * GeV;
  static const float mass_W = 80.399 * GeV;
  static const float mass_Z = 91.1876 * GeV;
  static const float mass_H = 125 * GeV;

  // cuts for charged particles
  double cfs_minpt = 5.0*GeV;
  double cfs_maxabseta = 4.5;



  // cuts for charged leptons
  double lfs_minpt = 5.0*GeV;
  double lfs_maxabseta = 4.5;


  // cuts for electrons (TopParticleLevel: 30*GeV, 2.5)
  double el_minpt = 30.0*GeV;
  double el_maxabseta = 2.5;

  //double el_maxabseta = 4.5;

  // cuts for muons (TopParticleLevel: 20*GeV, 2.5)
  double mu_minpt = 30.0*GeV;
  double mu_maxabseta = 2.5;

  // cuts for taus
  double tau_minpt = 25.0*GeV;
  double tau_maxabseta = 2.5;

  // cuts for neutrinos
  double nu_maxabseta = 4.5;

  // cuts for jets (TopParticleLevel: 20*GeV, 4.5)
  double jet_minpt = 30.0*GeV;
  double jet_maxabseta = 4.5;
  double bjet_maxabseta = 2.5;

  // cut for MET
  double met_min = 50.0*GeV;

  // cut for mT(W)
  double mtw_min = 60.0*GeV;

  // cut for abs(eta(light-jet))
  double lightjet_minabseta = 2;

 

  
  // cut for abs(delta_eta(light-jet, b-jet))
  double jets_minabsdeltaphi = 1.5; 

  // cut deltaR(lep, leadingJet) Signal Region                                                                                                                                       
  double deltaRLepLeadJet = 0.8;


  // cut for MET   Signal Region monotop                                                                                                                                                                                                                                                
  double met_min_signal = 120.0*GeV;  // monotop         




  /// @brief ATLAS MC single top-quark truth analysis
  /// @author Carlos Escobar <cescobar@cern.ch>

  class MC_monotop_Truth : public Analysis {
  public:

    /// Constructor
    MC_monotop_Truth()
      : Analysis("MC_monotop_Truth")
    {}

    // Analysis methods

    //=====================================================================
    // Set up projections and book histograms
    //=====================================================================
    void init() {

      cross_section = -1;

      // analysis summary
      MSG_INFO(summary());
      MSG_INFO(description());

      // read configuration file
      ReadConfigFile("MC_monotop_Truth.config");

      // set cross-section if requested
      if (cross_section > 0) { setCrossSection(cross_section); setNeedsCrossSection(true); }
      else { setNeedsCrossSection(false); }

      MSG_DEBUG("needsCrossSection() = " << needsCrossSection());
      if (needsCrossSection()) { MSG_INFO("crossSection() = " << crossSection() << " pb"); }

      // initialise projections before the run

      // FinalState projector to select all particles in |eta| < 4.5
      const FinalState fs(Cuts::abseta < 4.5);
      addProjection(fs, "fs");

      // ------------------------------------------------------------

      // charge final-state particles
      ChargedFinalState cfs((Cuts::abseta < cfs_maxabseta) & (Cuts::pT >= cfs_minpt));
      addProjection(cfs, "CFS");

    

      ChargedLeptons lfs(FinalState(-lfs_maxabseta, lfs_maxabseta, lfs_minpt));
      addProjection(lfs, "LFS");

    


  // ------------------------------------------------------------
      
      // Prompt definition: final state particles directly connected to the hard process.

      // projection to find the photons (for dressing leptons)
      IdentifiedFinalState photons_id(fs);
      photons_id.acceptIdPair(PID::PHOTON);
      addProjection(photons_id, "photons");
      PromptFinalState photons(photons_id);
      addProjection(photons, "photons_prompt");

      // ------------------------------------------------------------

      // projection to find final-state electrons
      IdentifiedFinalState electrons((Cuts::abseta < el_maxabseta) & (Cuts::pT >= el_minpt));
      electrons.acceptIdPair(PID::ELECTRON);
      addProjection(electrons, "electrons");

      // projection to find prompt electrons
      IdentifiedFinalState electrons_id(fs);
      electrons_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons_prompt(electrons_id);
      electrons_prompt.acceptTauDecays(true); // accept particles from decays of prompt taus as themselves being prompt
      addProjection(electrons_prompt, "electrons_prompt");

      // projection to find final-state muons
      IdentifiedFinalState muons((Cuts::abseta < el_maxabseta) & (Cuts::pT >= el_minpt));
      muons.acceptIdPair(PID::MUON);
      addProjection(muons, "muons");

      // projection to find prompt muons
      IdentifiedFinalState muons_id(fs);
      muons_id.acceptIdPair(PID::MUON);
      PromptFinalState muons_prompt(muons_id);
      muons_prompt.acceptTauDecays(true); // accept particles from decays of prompt taus as themselves being prompt
      addProjection(muons_prompt, "muons_prompt");

      // Dressed electrons
      DressedLeptons electrons_dressed(photons, electrons_prompt, 0.1, (Cuts::abseta < el_maxabseta) & (Cuts::pT >= el_minpt), true, false);
      addProjection(electrons_dressed, "electrons_dressed");

      // Dressed muons
      DressedLeptons muons_dressed(photons, muons_prompt, 0.1, (Cuts::abseta < mu_maxabseta) & (Cuts::pT >= mu_minpt), true, false);
      addProjection(muons_dressed, "muons_dressed");

      // ------------------------------------------------------------

      // projection to find the taus (selection on the final unstable state projector)
      UnstableFinalState taus((Cuts::abseta < tau_maxabseta) & (Cuts::pT >= tau_minpt));
      addProjection(taus, "taus");

      // ------------------------------------------------------------

      // projection to find neutrinos
      IdentifiedFinalState neutrinos_id(Cuts::abseta < nu_maxabseta);
      neutrinos_id.acceptNeutrinos();
      addProjection(neutrinos_id, "neutrinos");
      PromptFinalState neutrinos(neutrinos_id);
      addProjection(neutrinos, "neutrinos_prompt");
      neutrinos.acceptTauDecays(true); // accept particles from decays of prompt taus as themselves being prompt

      // ------------------------------------------------------------

      // FinalState used as input for jet-finding (including everything except bare electrons and muons, and neutrinos)
      VetoedFinalState jets_input;
      //jets_input.addVetoOnThisFinalState(electrons_prompt);
      //jets_input.addVetoOnThisFinalState(muons_prompt);
      //jets_input.addVetoOnThisFinalState(neutrinos); // veto on neutrinos prompt
     
      jets_input.addVetoOnThisFinalState(electrons_dressed);
      jets_input.addVetoOnThisFinalState(muons_dressed);
      jets_input.addVetoOnThisFinalState(neutrinos);
      FastJets jets = FastJets(jets_input, FastJets::ANTIKT, 0.4);
      // definition: useInvisibles (InvisiblesStrategy useinvis=DECAY_INVISIBLES)
      // options: NO_INVISIBLES, DECAY_INVISIBLES or ALL_INVISIBLES
      jets.useInvisibles();
      addProjection(jets, "jets");

      // ChargedLeptons: input for the jets including muons and neutrinos
      // A FinalState used to select particles within |eta| < 4.5 and with pT
      // > 30 GeV (if needed), out of which the ChargedLeptons projection picks only the
      // electrons and muons, to be accessed later as "LFS".
      //ChargedLeptons lfs(FinalState(-4.5, 4.5, 25.0*GeV));
      //addProjection(lfs, "LFS");

      // book histograms
      BookHistograms();
    }

    //=====================================================================
    // Perform the per-event analysis
    //=====================================================================
    void analyze(const Event& event) {
      
      // bool _debug=false;
      MSG_DEBUG("Event number: " << eventNumber << " (event number in file: " << event.genEvent()->event_number() << ")");

      _weight = event.weight();
      _h_weight->fill(_weight);
      MSG_DEBUG("MC weight: " << _weight);

      // ------------------------------------------------------------

      // fill charged particles
      const ChargedFinalState& _chargedParticles = applyProjection<ChargedFinalState>(event, "CFS");
      fillPropertiesFromCollection(_chargedParticles);
      
      // ------------------------------------------------------------


      // fill all photons
      const FinalState& _photons = applyProjection<FinalState>(event, "photons");
      vector<Particle> photons = fillPropertiesFromCollection(_photons, _h_photons);
      MSG_DEBUG("Number of photons: " << photons.size());
      // PrintFourMomentum(photons);

      // fill prompt photons
      const Particles& _photons_prompt = applyProjection<PromptFinalState>(event, "photons_prompt").particlesByPt();
      vector<Particle> photons_prompt = fillPropertiesFromCollection(_photons_prompt, _h_photons_prompt);
      MSG_DEBUG("Number of prompt photons: " << photons_prompt.size());
      PrintFourMomentum(photons_prompt);

      // ------------------------------------------------------------

      // fill final-state electron collection
      const FinalState& _electrons = applyProjection<FinalState>(event, "electrons");
      vector<Particle> good_electrons = fillPropertiesFromCollection(_electrons, _h_electrons);
      PrintFourMomentum(good_electrons);

      // fill prompt electrons
      const Particles& _electrons_prompt = applyProjection<PromptFinalState>(event, "electrons_prompt").particlesByPt();
      vector<Particle> good_electrons_prompt = fillPropertiesFromCollection(_electrons_prompt, _h_electrons_prompt, el_maxabseta, el_minpt);
      PrintFourMomentum(good_electrons_prompt);

      // fill dressed electrons
      const vector<DressedLepton>& _electrons_dressed = applyProjection<DressedLeptons>(event, "electrons_dressed").dressedLeptons();
      vector<DressedLepton> good_electrons_dressed = fillPropertiesFromCollection(_electrons_dressed, _h_electrons_dressed);
      PrintFourMomentum(good_electrons_dressed);

      // ------------------------------------------------------------

      // fill final-state muon collection
      const FinalState& _muons = applyProjection<FinalState>(event, "muons");
      vector<Particle> good_muons = fillPropertiesFromCollection(_muons, _h_muons);
      PrintFourMomentum(good_muons);

      // fill prompt muons
      const Particles& _muons_prompt = applyProjection<PromptFinalState>(event, "muons_prompt").particlesByPt();
      vector<Particle> good_muons_prompt = fillPropertiesFromCollection(_muons_prompt, _h_muons_prompt, mu_maxabseta, mu_minpt);
      PrintFourMomentum(good_muons_prompt);

      // fill dressed muons
      const vector<DressedLepton>& _muons_dressed = applyProjection<DressedLeptons>(event, "muons_dressed").dressedLeptons();
      vector<DressedLepton> good_muons_dressed = fillPropertiesFromCollection(_muons_dressed, _h_muons_dressed);
      PrintFourMomentum(good_muons_dressed);

      // ------------------------------------------------------------

      // fill dressed leptons (electrons + muons)
      vector<DressedLepton> good_leptons_dressed;
      good_leptons_dressed.reserve(_electrons_dressed.size() + _muons_dressed.size());
      good_leptons_dressed.insert(good_leptons_dressed.end(), _electrons_dressed.begin(), _electrons_dressed.end());
      good_leptons_dressed.insert(good_leptons_dressed.end(), _muons_dressed.begin(), _muons_dressed.end());
      sortByPt(good_leptons_dressed);
      PrintFourMomentum(good_leptons_dressed);


      
      
      
      
      // print dressing photons
      int numberOfDressingPhotons = 0;
      foreach(const DressedLepton _lepton_dressed, good_leptons_dressed) {
	foreach(const Particle& _photon, photons) {
	  // PrintFourMomentum(0, _photon);
	  // ignore photons from hadron/tau decays
	  // MSG_DEBUG(" -> is this photon coming from hadron or tau decay: " << (_photon.fromDecay() ? "yes" : "no"));
	  // MSG_DEBUG(" -> is this photon coming from hadron decay: " << (_photon.fromHadron() ? "yes" : "no"));
	  // MSG_DEBUG(" -> is this photon coming from prompt tau decay: " << (_photon.fromPromptTau() ? "yes" : "no"));
	  if (!_photon.fromDecay() && deltaR(_lepton_dressed, _photon) < 0.1) {
	  // if (!_photon.fromHadron() && !_photon.fromPromptTau(); && deltaR(_lepton_dressed, _photon) < 0.1) {
	  // if (!_photon.fromHadron() && deltaR(_lepton_dressed, _photon) < 0.1) {
	    PrintFourMomentum(0, _photon);
	    FillFourMomentum(_photon, _h_photons_fordressing);
	    numberOfDressingPhotons++;
	  }
	}
      }
      _h_photons_fordressing.at(0)->fill(numberOfDressingPhotons, _weight);

      // ------------------------------------------------------------

      // fill tau collection (pdg id)
      const UnstableFinalState& _taus = applyProjection<UnstableFinalState>(event, "taus");
      vector<Particle> good_taus = fillPropertiesFromCollection(_taus, _h_taus, 15);
      PrintFourMomentum(good_taus);

      // ------------------------------------------------------------

      // fill all neutrinos
      const FinalState& _neutrinos = applyProjection<FinalState>(event, "neutrinos");
      vector<Particle> good_neutrinos = fillPropertiesFromCollection(_neutrinos, _h_neutrinos);
      MSG_DEBUG("Number of neutrinos: " << good_neutrinos.size());
      PrintFourMomentum(good_neutrinos);

      // fill prompt neutrinos
      const Particles& _neutrinos_prompt = applyProjection<PromptFinalState>(event, "neutrinos_prompt").particlesByPt();
      vector<Particle> good_neutrinos_prompt = fillPropertiesFromCollection(_neutrinos_prompt, _h_neutrinos_prompt);
      MSG_DEBUG("Number of prompt neutrinos: " << good_neutrinos_prompt.size());
      PrintFourMomentum(good_neutrinos_prompt);

      // ------------------------------------------------------------

      // fill MET-like variable from all neutrino momenta and DM particle
     
      FourMomentum _me_dmP(0., 0., 0., 0.);
      vector<const HepMC::GenParticle*> _p_all = particles(event.genEvent());
      for (size_t i = 0; i < _p_all.size(); i++) {
	const HepMC::GenParticle* _p = _p_all[i];
	//cout<<" DM id numeber : "<<_p->pdg_id()<<endl;
	//

	if(_p->pdg_id()==1000022) { 
       	  //if(_p->pdg_id()==32) {
	  //cout<<" DM id numeber : "<<_p->pdg_id()<<endl;
	  const Particle  _dmParticle(_p);
	  _me_dmP += _dmParticle.momentum();
	  //cout<<" _me_dmP : "<<_me_dmP<<endl;
	}
      }


      FourMomentum _me_neutrino(0., 0., 0., 0.);
      foreach(const Particle& _neutrino, good_neutrinos) { _me_neutrino += _neutrino.momentum(); }
      
      FourMomentum _me_all(0., 0., 0., 0.);
      _me_all = _me_neutrino +  _me_dmP;

      _h_ME_all.at(0)->fill(_me_all.pT() /GeV, _weight);
      _h_ME_all.at(1)->fill(_me_all.eta(), _weight);
      _h_ME_all.at(2)->fill(_me_all.phi(), _weight);
      PrintFourMomentum(0, "ME (all)", _me_all);
      FourMomentum _met_all = _me_all;
      _met_all.setPz(0.);
      _met_all.setE(_met_all.pT());
      _h_MET_all.at(0)->fill(_met_all.pT()/GeV, _weight);
      _h_MET_all.at(1)->fill(_met_all.phi(), _weight);
      PrintFourMomentum(0, "MET (all)", _met_all);

      // -----------------------------------------------------------------------------------------------

      // fill MET-like variable from prompt neutrino momenta (to stay close to the hard process)
      FourMomentum _me_prompt_neutrino(0., 0., 0., 0.);
      foreach(const Particle& _neutrino_prompt, good_neutrinos_prompt) { _me_prompt_neutrino += _neutrino_prompt.momentum(); }
      
      FourMomentum _me_prompt(0., 0., 0., 0.);

      _me_prompt =  _me_prompt_neutrino + _me_dmP;

      _h_ME_prompt.at(0)->fill(_me_prompt.pT()/GeV, _weight);
      _h_ME_prompt.at(1)->fill(_me_prompt.eta(), _weight);
      _h_ME_prompt.at(2)->fill(_me_prompt.phi(), _weight);
      PrintFourMomentum(0, "ME (prompt neutrinos + DM)", _me_prompt);
      FourMomentum _met_prompt = _me_prompt;
      _met_prompt.setPz(0.);
      _met_prompt.setE(_met_prompt.pT());
      
      _h_MET_prompt.at(0)->fill(_met_prompt.pT()/GeV, _weight);
      _h_MET_prompt.at(1)->fill(_met_prompt.phi(), _weight);
      PrintFourMomentum(0, "MET (prompt neutrinos + DM)", _met_prompt);

      // ------------------------------------------------------------

      // fill MET (from sum of all or prompt neutrinos) vs. pT (from sum of prompt neutrinos)
      if (good_neutrinos_prompt.size() > 0) { _h_METalldivMEPT->fill(_met_all.pT()/_me_prompt.pT(), _weight); }
      // if (good_neutrinos_prompt.size() > 0) { _h_METdivMEPT->fill(_met_prompt.pT()/_me_prompt.pT(), _weight); }

      // ------------------------------------------------------------

      vector<Particle> B_hadrons = fillBHadrons(event);
      MSG_DEBUG("Number of B hadrons: " << B_hadrons.size());
      PrintFourMomentum(B_hadrons);

      // ------------------------------------------------------------


       





      // fill all jets with pT > 7 GeV (ATLAS standard jet collection)
      const Jets& _all_jets = applyProjection<FastJets>(event, "jets").jetsByPt((Cuts::abseta < jet_maxabseta) & (Cuts::pT > 7*GeV));

    
      Jets all_jets, good_jets;
    
      foreach(const Jet& jet, _all_jets) {
	bool overlap = false;
	foreach(const DressedLepton lepton_dressed, good_leptons_dressed) { overlap |= deltaR(jet, lepton_dressed) < 0.2; }
	if (!overlap) {
	  if (jet.momentum().pT() > jet_minpt) { good_jets += jet; }
	  all_jets += jet;
	}
      }
      fillPropertiesFromCollection(good_jets, _h_jets);

      //PrintFourMomentum("good_jet", good_jets);
      //PrintFourMomentum("all_jet", all_jets);

      Jets all_bjets, all_ljets, good_bjets, good_ljets;
      foreach(const Jet& jet, all_jets) {
        // Use b-tagging to partition the jets into b- and light-jets
        bool tagged = false;
	// look for b particles which have been tag-matched to a given jet
        foreach(const Particle& b, jet.bTags()) { tagged |= b.pT() > 5*GeV; }
        if (tagged) {
	  if (jet.momentum().pT() > jet_minpt && fabs(jet.momentum().eta()) < bjet_maxabseta) { good_bjets += jet; }
	  all_bjets += jet;
	}
	else {
	  if (jet.momentum().pT() > jet_minpt) { good_ljets += jet; }
	  all_ljets += jet;
	}
      }

      

      



      fillPropertiesFromCollection(all_ljets, _h_all_ljets);
      fillPropertiesFromCollection(all_bjets, _h_all_bjets);
      fillPropertiesFromCollection(good_ljets, _h_ljets);
      fillPropertiesFromCollection(good_bjets, _h_bjets);
      fillPropertiesFromCollection(good_leptons_dressed, _h_leptons_dressed);

      // PrintFourMomentum("light-jet (no cuts)", all_bjets);
      // PrintFourMomentum("b-jet (no cuts)", all_bjets);
      PrintFourMomentum("light-jet", good_ljets);
      PrintFourMomentum("b-jet", good_bjets);


      // fill charge lepton for monotop anas
      foreach(const Particle& lep, good_leptons_dressed){
	if (charge(lep)<0) {_h_leptons_dressed.at(7)->fill(0.5,_weight);}
	else if (charge(lep)>0) {_h_leptons_dressed.at(7)->fill(1.5,_weight);} 
      }
        
      // fill electron charge
      foreach(const Particle& elec, good_electrons_dressed){
        if (charge(elec)<0) {_h_electrons_dressed.at(7)->fill(0.5,_weight);}
        else if (charge(elec)>0) {_h_electrons_dressed.at(7)->fill(1.5,_weight);}
      }


      // fill muon charge                                                                                                                                                       
      foreach(const Particle& muons, good_muons_dressed){
        if (charge(muons)<0) {_h_muons_dressed.at(7)->fill(0.5,_weight);}
        else if (charge(muons)>0) {_h_muons_dressed.at(7)->fill(1.5,_weight);}
      }


      // jet topology 

      //deltaR(goodjets, goodLepton) (mu, e)  monotop anas
      if (good_jets.size()==1){
        _h_exclusive_leadingJet.at(0)->fill(good_jets[0].pT(),_weight);
      }
      if ( good_jets.size()>0) {
        _h_leadingJet.at(0)->fill(good_jets[0].pT(),_weight);
      }
      if ( good_jets.size()>1) {
        _h_leadingJet.at(1)->fill(good_jets[1].pT(),_weight);
        _h_DeltaRJetJet.at(0)->fill(deltaR(good_jets[0].momentum(),good_jets[1].momentum()) , _weight);
      }
        

      if ( good_leptons_dressed.size()>0 and good_jets.size()>0){
        _h_DeltaPhiLepLeadingJet.at(0)->fill( deltaPhi(good_leptons_dressed[0].momentum(), good_jets[0].momentum()), _weight);
        _h_DeltaRLepLeadingJet.at(0)->fill( deltaR(good_leptons_dressed[0].momentum(), good_jets[0].momentum()), _weight);
      }

     
      







      // -----------------------------------------------------------------------------------------------

      // reconstruct the W boson
      pair <Particle, double> WbosonPair = recoWboson(good_leptons_dressed, _met_prompt);

      // fill Second b-jet and b-jet from top-quark
      //pair <FourMomentum, FourMomentum> bjetPair = findbjetfromTopQuark(WbosonPair.first.momentum(), all_bjets);
      //FourMomentum myNULL(0., 0., 0., 0.);
      // if (bjetPair.second != myNULL) { FillFourMomentum(bjetPair.second, _h_bjet_second, 0, false); }

      // -----------------------------------------------------------------------------------------------
      // particle-level event pre-selection
      // -----------------------------------------------------------------------------------------------

      vector<string> namePreSelection;
      std::bitset<32> PreSelectionBitSet;
      //cout << "PreSelectionBitSet.to_ulong() = " << PreSelectionBitSet.to_ulong() << endl;

      // require exactly one lepton (electron or muon)
      namePreSelection.push_back(" 1 lepton (electron or muon)");
      if (good_leptons_dressed.size() != 1) { PreSelectionBitSet[namePreSelection.size()-1] = 0; }
      else { PreSelectionBitSet[namePreSelection.size()-1] = 1; }

      // require light-jet (spectator jet)
      namePreSelection.push_back(" 1 light-jet (spectator jet)");
      if (good_ljets.size() < 1) { PreSelectionBitSet[namePreSelection.size()-1] = 0; }
       else { PreSelectionBitSet[namePreSelection.size()-1] = 1; }
     
      // require jet (spectator jet)                                                                                                                                   
      //namePreSelection.push_back(" 1 light-jet (spectator jet)");
      //if (good_jets.size() < 1) { PreSelectionBitSet[namePreSelection.size()-1] = 0; }
      //else { PreSelectionBitSet[namePreSelection.size()-1] = 1; }
      
      
      // require exactly one b-jet (coming from top-quark decay)
      namePreSelection.push_back(" 1 b-jet (coming from top-quark decay)");
      if (good_bjets.size() != 1) { PreSelectionBitSet[namePreSelection.size()-1] = 0; }
      else { PreSelectionBitSet[namePreSelection.size()-1] = 1; }

      // require MET to be greater than met_min
      namePreSelection.push_back(" MET > " + Utils::ToString(met_min) + " GeV");
      PrintFourMomentum(0, "MET", _met_prompt);
      if (_met_prompt.pT() < met_min) { PreSelectionBitSet[namePreSelection.size()-1] = 0; }
      else { PreSelectionBitSet[namePreSelection.size()-1] = 1; }

      // require mT(lepton, MET) to be greater than mtw_min
      namePreSelection.push_back(" mT(lepton, MET) > " + Utils::ToString(mtw_min) + " GeV");
      double MTW = 0.0*GeV;
      if (good_leptons_dressed.size() > 0) {
	MTW = transverseMass(good_leptons_dressed[0].momentum(), _met_prompt); // in GeV
	MSG_DEBUG(" mT(lepton, MET) = " << MTW <<  " GeV");
      }
      if (MTW < mtw_min) { PreSelectionBitSet[namePreSelection.size()-1] = 0; }
      else { PreSelectionBitSet[namePreSelection.size()-1] = 1; }
     
     // -----------------------------------------------------------------------------------------------

      bool passPreSelection = true;
      MSG_DEBUG("=============================================================");
      MSG_DEBUG(" Preselection cuts");
      MSG_DEBUG("=============================================================");
      for (unsigned int i=0; i<namePreSelection.size(); i++) {
	MSG_DEBUG(namePreSelection[i] << " = " << ((PreSelectionBitSet[i]) ? "True" : "False"));
	if (PreSelectionBitSet[i] == 0) { passPreSelection = false; }
      }
      MSG_DEBUG("=============================================================");

      // -----------------------------------------------------------------------------------------------
      
      if (passPreSelection) {

	// fill electron or muon
	if (good_electrons_dressed.size() == 1) {
	  FillFourMomentum(good_electrons_dressed[0], _h_objectmap["PreSel_electron"], 0, false);
	  PrintFourMomentum(good_electrons_dressed);
	}
	if (good_muons_dressed.size() == 1) {
	  FillFourMomentum(good_muons_dressed[0], _h_objectmap["PreSel_muon"], 0, false);
	  PrintFourMomentum(good_muons_dressed);
	}

	// Fill lepton 
	
	FillFourMomentum(good_leptons_dressed[0], _h_objectmap["PreSel_lepton"], 0, false);

	// fill light-jet and b-jet
	FillFourMomentum(good_ljets[0], _h_objectmap["PreSel_ljet"], 0, false);
	FillFourMomentum(good_bjets[0], _h_objectmap["PreSel_bjet"], 0, false);
	FillFourMomentum(good_jets[0], _h_objectmap["PreSel_jet"], 0, false);
	PrintFourMomentum("light-jet", good_ljets);
	PrintFourMomentum("b-jet", good_bjets);



	// fill MET
	_h_objectmap["PreSel_MET"].at(0)->fill(_met_prompt.pT()/GeV, _weight);
	_h_objectmap["PreSel_MET"].at(1)->fill(_met_prompt.phi(), _weight);
	PrintFourMomentum(0, "MET", _met_prompt);

	// fill mT(lepton, MET)
	_h_objectmap["PreSel_MTW"].at(0)->fill(MTW, _weight);
	MSG_DEBUG("mT(lepton, MET) = " << MTW <<  " GeV");
	

	// fill mT(lepton, MET) + MET 
	//if((_met_prompt.pT()/GeV)>60 and (fabs(_met_prompt.eta())<2.5)) {
	_h_objectmap["PreSel_MTWMET"].at(0)->fill( MTW + _met_prompt.pT()/GeV, _weight);
	MSG_DEBUG("mT(lepton, MET) + MET = " << MTW + _met_prompt.pT()/GeV <<  " GeV");
	
    // }




       
	// fill HT
	double HT = _met_prompt.pT()/GeV;
	foreach(const DressedLepton lepton_dressed, good_leptons_dressed) { HT += lepton_dressed.momentum().pT()/GeV; }
	foreach(const Jet& j, good_jets) { HT += j.momentum().pT()/GeV; }
	MSG_DEBUG("HT = " << HT <<  " GeV");
	_h_objectmap["PreSel_HT"].at(0)->fill(HT, _weight);

	

	// fill leading jet 
	if (good_jets.size()>0) { _h_objectmap["PreSel_leadingJet"].at(0)->fill(good_jets[0].pT(), _weight);}
	

	

	// fill jet1, jet2 DeltaR
	if (good_jets.size()>1) { _h_objectmap["PreSel_DeltaRJetJet"].at(0)->fill(deltaR(good_jets[0].momentum(), good_jets[1].momentum()), _weight);}
	else if ( good_jets.size()==1 and good_ljets.size()>0)  { _h_objectmap["PreSel_DeltaRJetJet"].at(0)->fill(deltaR(good_jets[0].momentum(), good_ljets[0].momentum()), _weight);}
	
	// fill jet1, bjet DeltaR
	if (good_jets.size()>0 and good_bjets.size()>0) { _h_objectmap["PreSel_DeltaRJetBJet"].at(0)->fill(deltaR(good_jets[0].momentum(), good_bjets[0].momentum()), _weight);}

	// fill DeltaPhi and DeltaR leadingJet and bjet
	if(good_electrons_dressed.size()==1 and good_jets.size()>0) {
	  _h_objectmap["PreSel_DeltaPhiLepLeadingJet_el"].at(0)->fill(deltaPhi(good_jets[0].momentum(), good_electrons_dressed[0].momentum()), _weight);
	  _h_objectmap["PreSel_DeltaRLepLeadingJet_el"].at(0)->fill(deltaR(good_jets[0].momentum(), good_electrons_dressed[0].momentum()), _weight);
	 
	}
	
	if(good_muons_dressed.size()==1 and good_jets.size()>0) {
          _h_objectmap["PreSel_DeltaPhiLepLeadingJet_mu"].at(0)->fill(deltaPhi(good_jets[0].momentum(), good_muons_dressed[0].momentum()), _weight);
	  _h_objectmap["PreSel_DeltaRLepLeadingJet_mu"].at(0)->fill(deltaR(good_jets[0].momentum(), good_muons_dressed[0].momentum()), _weight);
        }

	if (good_leptons_dressed.size()>0 and good_jets.size()>0) {
	  _h_objectmap["PreSel_DeltaPhiLepLeadingJet"].at(0)->fill(deltaPhi(good_jets[0].momentum(),good_leptons_dressed[0].momentum()), _weight);
	  _h_objectmap["PreSel_DeltaRLepLeadingJet"].at(0)->fill(deltaR(good_jets[0].momentum(), good_leptons_dressed[0].momentum()), _weight);
	}
	
	
	// fill DeltaPhi and DeltaR bjet and lepton 
	
	//cout<<"Presel good BJETs size : " <<good_bjets.size()<<endl;
	//cout<<"Presel good_electrons_dressed.size :"<<good_electrons_dressed.size()<<endl;
	//cout<<"Presel good_jets.size : "<<good_jets.size()<<endl;
	
	// fill PreSel_GoodJets_N

	//fillPropertiesFromCollection(good_jets, _h_presel_jets);
       


	if(good_electrons_dressed.size()>0 and good_bjets.size()>=1) {
          _h_objectmap["PreSel_DeltaPhiLepBJet_el"].at(0)->fill(deltaPhi(good_bjets[0].momentum(), good_electrons_dressed[0].momentum()), _weight);
          _h_objectmap["PreSel_DeltaRLepBJet_el"].at(0)->fill(deltaR(good_bjets[0].momentum(), good_electrons_dressed[0].momentum()), _weight);

        }

        if(good_muons_dressed.size()>0 and good_bjets.size()>=1) {
          _h_objectmap["PreSel_DeltaPhiLepBJet_mu"].at(0)->fill(deltaPhi(good_bjets[0].momentum(), good_muons_dressed[0].momentum()), _weight);
          _h_objectmap["PreSel_DeltaRLepBJet_mu"].at(0)->fill(deltaR(good_bjets[0].momentum(), good_muons_dressed[0].momentum()), _weight);
        }

        if (good_leptons_dressed.size()>0 and good_bjets.size()>=1) {
          _h_objectmap["PreSel_DeltaPhiLepBJet"].at(0)->fill(deltaPhi(good_bjets[0].momentum(),good_leptons_dressed[0].momentum()), _weight);
          _h_objectmap["PreSel_DeltaRLepBJet"].at(0)->fill(deltaR(good_bjets[0].momentum(), good_leptons_dressed[0].momentum()), _weight);
        }

       
	//fillPropertiesFromCollection(good_jets, _h_presel_jets);
	//_h_objectmap["PreSel_GoodJets"].at(0)->fill(good_jets.size(), _weight);
	// fill W boson
	FillFourMomentum(WbosonPair.first, _h_objectmap["PreSel_W"], 0, true);
	_h_objectmap["PreSel_W"].at(7)->fill(WbosonPair.second, _weight);
	PrintFourMomentum(0, WbosonPair.first);

	numberOfPreselectedEvents++;

	// -----------------------------------------------------------------------------------------------
	// particle-level event selection
	// -----------------------------------------------------------------------------------------------
	
	vector<string> nameSelection;
	std::bitset<32> SelectionBitSet;
	
	// Selecction Signal Monotop ( My Selecction: Florencia)

	// require exactly two jets (2 goodjets = 1 ¡b-jets and 1 light-jet) (spectator jet)
                  
	
	nameSelection.push_back(" 1 jet and 1 light-jet or 2 jet (spectator jet)");                                   
	//if (good_jets.size() < 2) { SelectionBitSet[nameSelection.size()-1] = 0; }
        if (good_ljets.size() != 1 and good_bjets.size() != 1) { SelectionBitSet[nameSelection.size()-1] = 0;} 
	else { SelectionBitSet[nameSelection.size()-1] = 1; }

                                                                 
                                                  
                                                                 
	// DeltaR(lep, leadingJet)
	nameSelection.push_back(" |DeltaR(lep, leadingJet)| < " );
	if (deltaR(good_jets[0].momentum(), good_leptons_dressed[0].momentum()) > deltaRLepLeadJet) { SelectionBitSet[nameSelection.size()-1] = 0; }  
	else { SelectionBitSet[nameSelection.size()-1] = 1; } 
	
	// MET cut 
	nameSelection.push_back(" MET > " + Utils::ToString(met_min_signal) + " GeV"); 
	PrintFourMomentum(0, "MET", _met_prompt);
	if (_met_prompt.pT() < met_min_signal) { SelectionBitSet[nameSelection.size()-1] = 0; }  
	else { SelectionBitSet[nameSelection.size()-1] = 1; } 
                                                                                                                                    
	
	// -----------------------------------------------------------------------------------------------
	
	bool passSelection = true;
	MSG_DEBUG("=============================================================");
	MSG_DEBUG(" Selection cuts");
	MSG_DEBUG("=============================================================");
	for (unsigned int i=0; i<nameSelection.size(); i++) {
	  MSG_DEBUG(nameSelection[i] << " = " << ((SelectionBitSet[i]) ? "True" : "False"));
	  if (SelectionBitSet[i] == 0) { passSelection = false; }
	}
	MSG_DEBUG("=============================================================");
	
	// -----------------------------------------------------------------------------------------------
	if (passSelection) {
	  //FillSignalDistributions("Sel", _met_prompt, good_jets, good_ljets, good_bjets, good_leptons_dressed);
	  // Filling plots Selection level


       
	  _h_signalmap["Sel_MET"]->fill(_met_prompt.pT()/GeV, _weight);
	  
	  _h_signalmap["Sel_leadingJet_pt"]->fill(good_jets[0].pT()/GeV, _weight);
	  
	  _h_signalmap["Sel_lepton_pt"]->fill(good_leptons_dressed[0].pT()/GeV, _weight);

	  _h_signalmap["Sel_DeltaRLepLeadingJet"]->fill(deltaR(good_leptons_dressed[0].momentum(), good_jets[0].momentum()), _weight);

   
	  _h_signalmap["Sel_DeltaRLepBJet"]->fill(deltaR(good_leptons_dressed[0].momentum(), good_bjets[0].momentum()), _weight);

	  
	  if (good_jets.size()>1) {
	    _h_signalmap["Sel_DeltaRJetJet"]->fill(deltaR(good_jets[0].momentum(), good_jets[1].momentum()), _weight);}
	  else if ( good_jets.size()==1 and good_ljets.size()>0 ) { 
	   _h_signalmap["Sel_DeltaRJetJet"]->fill(deltaR(good_jets[0].momentum(), good_ljets[0].momentum()), _weight);}
	  
	  

	 // fill mT(lepton, MET) + MET 
	_h_signalmap["Sel_MTWMET"]->fill( MTW + _met_prompt.pT()/GeV, _weight);
	MSG_DEBUG("mT(lepton, MET) + MET = " << MTW + _met_prompt.pT()/GeV <<  " GeV");
	





	  numberOfSelectedEvents++;




	}
      }

      // MSG_DEBUG("");
      eventNumber++;
    } // end of analyze()
    
    //=====================================================================
    // finalize
    //=====================================================================
    void finalize() {

      MSG_INFO("=============================================================");
      // MSG_INFO(" number of events = " << numEvents());
      MSG_INFO(" number of events = " << eventNumber);
      MSG_INFO(" number of pre-selected events = " << numberOfPreselectedEvents);
      MSG_INFO(" number of selected events = " << numberOfSelectedEvents);
      if (needsCrossSection()) {
	MSG_INFO(" cross-section = " << cross_section << " pb :: sumOfWeights = " << sumOfWeights());
	MSG_INFO(" luminosity = " << numEvents()/cross_section << " pb-1");
      }

      MSG_INFO("=============================================================");
      // scale(histo, crossSection()/femtobarn/sumOfWeights());
    }

   
    //=====================================================================
    // Filling Singla Distributions
    //=====================================================================
    void FillSignalDistributions( string histoNameSuffix, FourMomentum _met, FourMomentum _jet, FourMomentum _ljet, FourMomentum _bjet, FourMomentum _lepton) {
      _h_signalmap[histoNameSuffix+"_MET"]->fill(_met.pT()/GeV, _weight);
    }

     

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(string jetname, Jets _jets) {
      int index = 0;
      foreach(const Jet& j, _jets) {
    	PrintFourMomentum(index, jetname, j.momentum());
    	index++;
      }
    }
  
    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(vector<HepMC::GenParticle*> _partvect) {
      int index = 0;
      foreach (HepMC::GenParticle* _p, _partvect) {
     	PrintFourMomentum(index, Particle(*_p));
     	index++;
      }
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(vector<Particle> _partvect) {
      int index = 0;
      foreach (Particle _p, _partvect) {
	// cout << "_p.momentum().phi(): " << _p.momentum().phi(MINUSPI_PLUSPI) << " rad" << endl;
	// cout << _p.pid() << endl;
     	// PrintFourMomentum(index, PrintPdgId(_p.pid()), _p);
     	PrintFourMomentum(index, PrintPdgId(_p.pdgId()), _p);
     	index++;
      }
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(int index, Particle _part) {
      PrintFourMomentum(index, PrintPdgId(_part.pid()), _part);
    }
    
    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(vector<DressedLepton> _part) {
      int index = 0;
      foreach (DressedLepton _p, _part) { 
	// cout << _p.pid() << endl;
	PrintFourMomentum(index, PrintPdgId(_p.pid()), _p.momentum());
	index++;
      }
    }

    //=====================================================================
    // Mass
    //=====================================================================
    float Mass(Particle _p) { return Mass(_p.momentum()); }
    
    //=====================================================================
    // Mass
    //=====================================================================
    float Mass(FourMomentum FourMom) {
      float mass = sqrt(FourMom.mass2())/GeV;
      if (Rivet::isZero(mass, 1E-9)) { mass = 0.; }
      if (std::isnan(mass)) { mass = 0.; }
      return mass;
    }

    //=====================================================================
    // PrintPdgId
    //=====================================================================
    string PrintPdgId(PdgId _p) {
      string partidname = "";
      if (fabs(_p)==11) { partidname = "Electron (" + to_string(_p); }
      else if (fabs(_p)==13) { partidname = "Muon (" + to_string(_p); }
      else if (fabs(_p)==15) { partidname = "Tau (" + to_string(_p); }
      else if (fabs(_p)==12 || fabs(_p)==14 || fabs(_p)==16) { partidname = "Neutrino (" + to_string(_p); }
      else if (fabs(_p)==22) { partidname = "Photon (" + to_string(_p); }
      else if (fabs(_p)==24) { partidname = "W boson (" + to_string(_p); }
      else if (fabs(_p)==5) { partidname = "b quark (" + to_string(_p); }
      else if (fabs(_p)==6) { partidname = "Top quark (" + to_string(_p); }
      else { partidname = "(" + to_string(_p); }
      // else { partidname = patch::to_string(_p); }
      return partidname + ")";
    }

    //=====================================================================
    // PrintFourMomentum
    //=====================================================================
    void PrintFourMomentum(int index, string partname, FourMomentum FourMom) {
      /*
	MSG_DEBUG(index << ". (" << partname << ") " <<
	"pT = " << FourMom.pT()/GeV << " GeV, " <<
	"m = " << Mass(FourMom) << " GeV, " <<
	"E = " << FourMom.E()/GeV << " GeV, " <<
	"ET = " << FourMom.Et()/GeV << " GeV, " <<
	"p = [" << FourMom.px()/GeV << ", " << FourMom.py()/GeV << ", " << FourMom.pz()/GeV << "] GeV, " <<
	"\u03B7 = " << FourMom.eta() << ", "
	"\u0278 = " << FourMom.phi()<< " rad");
      */

      // cout << "Mass(FourMom) : " << Mass(FourMom) << " GeV" << endl;
      // cout << "FourMom.mass(): " << FourMom.mass()/GeV << " GeV" << endl;

      // In MathHeader.hh
      // Enum for range of phi to be mapped into (default: ZERO_2PI)
      // enum PhiMapping { MINUSPI_PLUSPI, ZERO_2PI, ZERO_PI };

      // cout << "FourMom.azimuthalAngle(): " << FourMom.azimuthalAngle(MINUSPI_PLUSPI) << " rad" << endl;
      // cout << "FourMom.phi(): " << FourMom.phi(MINUSPI_PLUSPI) << " rad" << endl;
      // cout << "FourMom.phi(): " << FourMom.phi() << " rad" << endl;

      MSG_DEBUG(index << ". " << partname << fixed << setprecision(3) << " "
	       << "pT, m, E, p = [" << FourMom.pT()/GeV << ", " << Mass(FourMom) << ", " << FourMom.E()/GeV
	       << " (" << FourMom.px()/GeV << ", " << FourMom.py()/GeV << ", " << FourMom.pz()/GeV << ") ] GeV; "
	       << "\u03B7, \u0278 = [ " << FourMom.eta() << ", " << FourMom.phi(MINUSPI_PLUSPI) << " rad ]");
    }

    //=====================================================================
    // FillFourMomentum
    //=====================================================================
    void FillFourMomentum(FourMomentum _p, vector<Histo1DPtr> _histo, int index=0, bool fillMass=false) {
      _histo.at(index)->fill(_p.E()/GeV, _weight);
      _histo.at(index+1)->fill(_p.Et()/GeV, _weight);
      _histo.at(index+2)->fill(_p.pT()/GeV, _weight);
      _histo.at(index+3)->fill(_p.pz()/GeV, _weight);
      _histo.at(index+4)->fill(_p.eta(), _weight);
      _histo.at(index+5)->fill(_p.phi(), _weight);
      if (fillMass) {
	// MSG_DEBUG("mass = " << _p.momentum().mass() << " GeV");
	MSG_DEBUG("mass = " << Mass(_p) << " GeV");
	_histo.at(index+6)->fill(Mass(_p), _weight);
      }
    }

    //=====================================================================
    // fill B-Hadrons
    //=====================================================================
    vector<Particle> fillBHadrons(const Event& event, float etaCut=4.5, float ptCut=5.0*GeV) {
      vector<Particle> B_hadrons;
      // Get the B-Hadrons with pT > ptCut GeV, to add to the final-state particles used for jet clustering.
      vector<const HepMC::GenParticle*> allParticles = particles(event.genEvent());
      for (size_t i = 0; i < allParticles.size(); i++) {
	const HepMC::GenParticle* _p = allParticles[i];
	// HepMC::GenParticle* _p = allParticles.at(i);
	
	// If the particle is a B-hadron and has pT > ptCut GeV, add it to the B-hadrons vector
	if(PID::isHadron(_p->pdg_id()) && PID::hasBottom(_p->pdg_id()) && fabs(_p->momentum().eta()) < etaCut && _p->momentum().perp() > ptCut) {
	  Particle B_hadron = Particle(*_p);
	  FillFourMomentum(B_hadron, _h_B_hadrons, 1, false);
	  B_hadrons.push_back(B_hadron);
	}
      }
      _h_B_hadrons.at(0)->fill(B_hadrons.size(), _weight);
      return B_hadrons;
    }

    //=====================================================================
    // fill properties from Collection - ChargedFinalState
    //=====================================================================
    vector<FourMomentum> fillPropertiesFromCollection(const ChargedFinalState& Collection, float etaCut=5.0, float ptCut=0.0*GeV) {
      vector<FourMomentum> _charged;
      foreach(const Particle& _p, Collection.particlesByPt()) {
	FourMomentum FourMom(0., 0., 0., 0.);
	FourMom = FourMom + _p.momentum();
	if (FourMom.pT() > ptCut && fabs(FourMom.eta()) < etaCut) {
	  _charged.push_back(FourMom);
	  _h_charged.at(1)->fill(FourMom.E()/GeV, _weight);
	  _h_charged.at(2)->fill(FourMom.Et()/GeV, _weight);
	  _h_charged.at(3)->fill(FourMom.pT()/GeV, _weight);
	  _h_charged.at(4)->fill(FourMom.pz()/GeV, _weight);
	  _h_charged.at(5)->fill(FourMom.eta(), _weight);
	  _h_charged.at(6)->fill(FourMom.phi(), _weight);
	  // _h_charged.at(7)->fill(Mass(FourMom), _weight);
	  // _h_charged.at(8)->fill(sqrt(FourMom.mass2()+pow(FourMom.px(),2)+pow(FourMom.py(),2)), _weight);
	}
      }
      MSG_DEBUG("Number of charged particles = " << _charged.size());
      _h_charged.at(0)->fill(Collection.size(),_weight);
      // _h_charged.at(0)->fill(_charged.size(), _weight);
      return _charged;
    }

    //=====================================================================
    // fill properties from Collection - FinalState
    //=====================================================================
    vector<Particle> fillPropertiesFromCollection(const FinalState& Collection, vector<Histo1DPtr> _histo, 
						  float etaCut=5.0, float ptCut=0.0*GeV) {
      vector<Particle> _partvect;
      foreach(const Particle& _p, Collection.particlesByPt()) {
	if (_p.momentum().pT() > ptCut && fabs(_p.momentum().eta()) < etaCut) {
	  
	  // exclude the transition region for electrons
	  // if (fabs(_p.pdgId())==11 && (fabs(_p.momentum().eta()) <= 1.52 && fabs(_p.momentum().eta()) >= 1.37)) { continue; }
	  
	  // if (fabs(_p.pdgId())==22) { continue; }

	  _partvect.push_back(_p);
	  FillFourMomentum(_p, _histo);
	  // PrintFourMomentum(_partvect.size(), _p);
	}
      }
      _histo.at(0)->fill(_partvect.size(), _weight);
      return _partvect;
    }

    //=====================================================================
    // fill properties from Collection - DressedLepton
    //=====================================================================
    vector<DressedLepton> fillPropertiesFromCollection(const vector<DressedLepton>& Collection, vector<Histo1DPtr> _histo, 
						       float etaCut=6.0, float ptCut=0.0*GeV) {
      vector<DressedLepton> _partvect;
      foreach(const DressedLepton& _p, Collection) {
	// foreach(const DressedLepton& _p, Collection.particlesByPt()) {
	if (_p.momentum().pT() > ptCut && fabs(_p.momentum().eta()) < etaCut) {
	  
	  // exclude the transition region for electrons
	  // if (fabs(_p.pdgId())==11 && (fabs(_p.momentum().eta()) <= 1.52 && fabs(_p.momentum().eta()) >= 1.37)) { continue; }
	  
	  _partvect.push_back(_p);
	  FillFourMomentum(_p, _histo);
	  // PrintFourMomentum(_partvect.size(), _p);
	}
      }
      _histo.at(0)->fill(_partvect.size(), _weight);
      sortByPt(_partvect);
      return _partvect;
    }

    //=====================================================================
    // fill properties from Collection - Particles
    //=====================================================================
    vector<Particle> fillPropertiesFromCollection(const Particles& Collection, vector<Histo1DPtr> _histo, float etaCut=6.0, float ptCut=0.0) {
      vector<Particle> _partvect;
      // Particles Collection is already sort particles by pT
      // foreach(const Particle& _p, Collection.particlesByPt()) {
      foreach(const Particle& _p, Collection) {
	if (_p.momentum().pT() > ptCut && fabs(_p.momentum().eta()) < etaCut) {
	  _partvect.push_back(_p);
	  FillFourMomentum(_p, _histo);
	  // PrintFourMomentum(_partvect.size(), _p);
	}
      }
      _histo.at(0)->fill(_partvect.size(), _weight);
      return _partvect;
    }

    //=====================================================================
    // fill properties from Collection - UnstableFinalState
    //=====================================================================
    vector<Particle> fillPropertiesFromCollection(const UnstableFinalState& Collection, vector<Histo1DPtr> _histo, 
						  int pdfId, float etaCut=5.0, float ptCut=0.0*GeV) {
      vector<Particle> _partvect;
      foreach(const Particle& _p, Collection.particlesByPt()) {
    	if(abs(_p.pdgId())!=pdfId) continue;
    	if (_p.momentum().pT() > ptCut && fabs(_p.momentum().eta()) < etaCut) {
    	  _partvect.push_back(_p);
    	  FillFourMomentum(_p, _histo);
    	  // PrintFourMomentum(_partvect.size(), _p);
    	}
      }
      _histo.at(0)->fill(_partvect.size(), _weight);
      return _partvect;
    }

    //=====================================================================
    // fill properties from Collection - Jets
    //=====================================================================
    void fillPropertiesFromCollection(const Jets& Collection, vector<Histo1DPtr> _histo, float ptCut=0.0*GeV, bool fillNumberOfJets = true) {
      int numberOfGoodJets = 0;
      foreach(const Jet& _jet, Collection) {
	if (_jet.momentum().pT() > ptCut) {
	  FillFourMomentum(_jet, _histo, ((fillNumberOfJets) ? 1 : 0), false);
	  // cout << ((fillNumberOfJets) ? 1 : 0) << ". E = " << _jet.momentum().E() << " GeV, "<< _jet.momentum().E()*_weight << " GeV" << endl;
	  // cout << ((fillNumberOfJets) ? 2 : 1) << ". ET = " << _jet.momentum().Et() << " GeV, "<< _jet.momentum().Et()*_weight << " GeV" << endl;
	  // cout << ((fillNumberOfJets) ? 3 : 2) << ". pT = " << _jet.momentum().pt() << " GeV, "<< _jet.momentum().pt()*_weight << " GeV" << endl;
	  // cout << ((fillNumberOfJets) ? 4 : 3) << ". pz = " << _jet.momentum().pz() << " GeV, "<< _jet.momentum().pz()*_weight << " GeV" << endl;
	  // cout << ((fillNumberOfJets) ? 5 : 4) << ". phi = " << _jet.momentum().phi() << " GeV, "<< _jet.momentum().phi()*_weight << " GeV" << endl;
	  // cout << ((fillNumberOfJets) ? 6 : 5) << ". eta = " << _jet.momentum().eta() << " GeV, "<< _jet.momentum().eta()*_weight << " GeV" << endl;
	  numberOfGoodJets++;
	}
	if (fillNumberOfJets) { _histo.at(0)->fill(numberOfGoodJets, _weight); }
      }
    }

    //=====================================================================
    // find second b-jet and b-jet from top-quark decay
    //=====================================================================
    pair <FourMomentum, FourMomentum> findbjetfromTopQuark(FourMomentum _Wboson, Jets _all_bjets) {

      FourMomentum myNULL(0., 0., 0., 0.);
      if (_Wboson == myNULL) { return make_pair(myNULL, myNULL); }
      
      Jets _good_bjets_noEtaCut;
      foreach(const Jet& jet, _all_bjets) {

	_good_bjets_noEtaCut += jet;
      }

      if (_good_bjets_noEtaCut.size() != 2) { return make_pair(FourMomentum(), FourMomentum()); }

      FourMomentum bjet_fromtop(0., 0., 0., 0.);
      FourMomentum bjet(0., 0., 0., 0.);

      FourMomentum topquark_candidate0 = _Wboson + _good_bjets_noEtaCut[0].momentum();
      FourMomentum topquark_candidate1 = _Wboson + _good_bjets_noEtaCut[1].momentum();

      PrintFourMomentum(0, "top-quark_candidate", topquark_candidate0);
      PrintFourMomentum(0, "b-jet candidate from top quark", _good_bjets_noEtaCut[0].momentum());

      PrintFourMomentum(1, "top-quark_candidate", topquark_candidate1);
      PrintFourMomentum(1, "b-jet candidate from top quark", _good_bjets_noEtaCut[1].momentum());

      if (fabs(topquark_candidate0.mass() - mass_t) < fabs(topquark_candidate1.mass() - mass_t)) {
	bjet_fromtop = _good_bjets_noEtaCut[0].momentum();
	bjet = _good_bjets_noEtaCut[1].momentum();
      }
      else {
	bjet_fromtop = _good_bjets_noEtaCut[1].momentum();
	bjet = _good_bjets_noEtaCut[0].momentum();
      }

      PrintFourMomentum(0, "b-jet (from top-quark decay)", bjet_fromtop);
      PrintFourMomentum(0, "b-jet", bjet);
      
      return make_pair(bjet_fromtop, bjet);
    }

    //=====================================================================
    // reconstruct W boson
    //=====================================================================
    pair <Particle, double> recoWboson(vector<DressedLepton> _leptons,  FourMomentum _met) {

      FourMomentum myNULL(0., 0., 0., 0.);
      if (_leptons.size() != 1) { return make_pair(Particle(-1, myNULL), -1.0); }

      DressedLepton _lepton = _leptons[0];

      // variables
      double MW2 =pow(mass_W, 2);
      double ML2 =pow(Mass(_lepton), 2);
      // cout << "_lepton.momentum().mass(): " << _lepton.momentum().mass() << " GeV" << endl;
      // cout << "Mass(_lepton): " << Mass(_lepton) << " GeV" << endl;
      double EL2 =pow(_lepton.momentum().E(), 2);
      double PxL = _lepton.momentum().px();
      double PyL = _lepton.momentum().py();
      double PzL = _lepton.momentum().pz();
      double PzL2 =pow(PzL, 2);
      double CosFn = cos(_met.phi());
      double SinFn = sin(_met.phi());
      double MET = _met.pT();
      
      double PxN = _met.px();
      double PyN = _met.py();

      // Using the method used in JHEP 04 (2016) 023, JHEP 04 (2017) 124 and arXiv:1707.05393

      // Terms to compute the neutrino pZ
      double Term_A = EL2 - PzL2;
      double Term_B = (ML2 - MW2 - 2*(PxL*PxN + PyL*PyN)) * PzL;
      double Term_Sqrt = EL2 * ( pow((-ML2 + MW2 + 2*(PxL*PxN + PyL*PyN)), 2) + 4 *pow(MET,2)*(-EL2 + PzL2) );
      
      // if sqrt term is negative, let's avoid nan's
      if (Term_Sqrt<0) {
	MSG_DEBUG("Term_Sqrt = " << Term_Sqrt);
	MSG_DEBUG("** Neutrino Pz **  we are in trouble boy! No straight solution for neutrino Pz");
	MSG_DEBUG("--> Estimating the MET that enables the solution");

	// try the Mathematica expressions
	double TermMET_a = EL2 - PzL2 - pow((PxL*CosFn + PyL*SinFn), 2);
	double TermMET_b = (ML2 - MW2) * (PxL*CosFn + PyL*SinFn);
	double TermMET_sqrt = pow((-MW2+ML2),2) * (EL2-PzL2);
	
	double NewMET_Sol[2] = {0.};

	NewMET_Sol[0] = (-TermMET_b+sqrt(TermMET_sqrt))/(2*TermMET_a);
	NewMET_Sol[1] = (-TermMET_b-sqrt(TermMET_sqrt))/(2*TermMET_a);

	MSG_DEBUG(" *** Initial MET: " << MET << " GeV -> trying to Fix MET... Two solutions found: MET_Sol1 = " << NewMET_Sol[0] 
		 << " GeV, MET_Sol2 = " << NewMET_Sol[1] << " GeV");

	// now choose the physical (positive) solution
	if (NewMET_Sol[0]>0. && NewMET_Sol[1]<0.) { MET = NewMET_Sol[0]; MSG_DEBUG("MET_1 selected"); }
	if (NewMET_Sol[0]<0. && NewMET_Sol[1]>0.) { MET = NewMET_Sol[1]; MSG_DEBUG("MET_2 selected"); }
	
	// if both solutions are negative, then there is a real problem with this event
	if (NewMET_Sol[0]<0. && NewMET_Sol[1]<0.) { MSG_ERROR("Negative MET!"); exit(1); }
	
	// if both solutions are positive, the closer solution to the initial MET is chosen
	if (NewMET_Sol[0]>0. && NewMET_Sol[1]>0.) {
	  MET = (fabs(MET-NewMET_Sol[0]) < fabs(MET-NewMET_Sol[1])) ? NewMET_Sol[0] : NewMET_Sol[1];
	}
	
	MSG_DEBUG(" *** Modified MET: " << MET << " GeV");
	
	// As the solutions make the discriminant (Term_Sqrt) zero, the MET is decreased even more to be safe
	// MET -= 0.00001; // in GeV
	while (Term_Sqrt<0) {
	  MET -= 0.000000001; // in GeV
	  // with the new MET one has to recompute the PxN, PyN and pow(MET,2) and the Term_B, Term_Sqrt
	  PxN = MET * CosFn;
	  PyN = MET * SinFn;
	  Term_Sqrt = EL2 *(pow((-ML2 + MW2 + 2*PxL*PxN + 2*PyL*PyN),2) + 4 *pow(MET,2)*(-EL2 + PzL2));
	}
	
	MSG_DEBUG("** Neutrino Pz **  new MET has been chosen; Input MET= " << _met.pT() << " GeV --> New MET= " << MET << " GeV");
	
	// Term_B is recomputed with the new PxN and PyN
	Term_B = (ML2 - MW2 - 2*PxL*PxN - 2 *PyL *PyN) * PzL;
      } // Term_Sqrt<0
      
      // double METprime = MET;

      // show final terms
      MSG_DEBUG("Term_A = " << Term_A);
      MSG_DEBUG("Term_B = " << Term_B);
      MSG_DEBUG("Term_Sqrt = " << Term_Sqrt);

      // Obtain the 2 solutions of the equation
      double NeutrinoPz_Sol[2] = {0.};
      NeutrinoPz_Sol[0] = (-Term_B + sqrt(Term_Sqrt))/(2*Term_A);
      NeutrinoPz_Sol[1] = (-Term_B - sqrt(Term_Sqrt))/(2*Term_A);
  
      MSG_DEBUG(" ** Positive solution --> new Pz = " << NeutrinoPz_Sol[0] << " GeV");
      MSG_DEBUG(" ** Negative solution --> new Pz = " << NeutrinoPz_Sol[1] << " GeV");
    
      // Construct the neutrino 4-mom to be returned (be ware in case of MET change)
      double NeutrinoE_Sol[2] = {0.};
      NeutrinoE_Sol[0] = sqrt(pow(MET*CosFn, 2) + pow(MET*SinFn, 2) + pow(NeutrinoPz_Sol[0], 2));
      NeutrinoE_Sol[1] = sqrt(pow(MET*CosFn, 2) + pow(MET*SinFn, 2) + pow(NeutrinoPz_Sol[1], 2));
  
      // Construct the two neutrino 4-mom solutions
      // FourMomentum (const double E, const double px, const double py, const double pz)
      FourMomentum Neutrino_Sol[2];

      vector<FourMomentum> _neutrino_candidate;
      _neutrino_candidate.push_back(FourMomentum(NeutrinoE_Sol[0], MET*CosFn, MET*SinFn, NeutrinoPz_Sol[0]));
      _neutrino_candidate.push_back(FourMomentum(NeutrinoE_Sol[1], MET*CosFn, MET*SinFn, NeutrinoPz_Sol[1]));
      PrintFourMomentum(0, "neutrino candidate", _neutrino_candidate[0]);
      PrintFourMomentum(1, "neutrino candidate", _neutrino_candidate[1]);

      // if two real solutions, then the solution giving the smallest magnitude of the longitudinal neutrino momentum is taken
      int selectedidx = -1;
      if (fabs(_neutrino_candidate[0].pz()) < fabs(_neutrino_candidate[1].pz())) { selectedidx = 0; }
      else { selectedidx = 1; }
      // cout << "selected index = " << selectedidx << endl;

      FourMomentum _reco_Wboson = _lepton.momentum() + _neutrino_candidate[selectedidx];
      // PrintFourMomentum(0, "reconstructed W boson", _reco_Wboson);
      Particle _Wboson(((_lepton.pdgId() > 0) ? -24 : 24), _reco_Wboson);
      PrintFourMomentum(0, _Wboson);
      // MSG_DEBUG("Mass(_Wboson): " << Mass(_Wboson) <<  " GeV");
      // MSG_DEBUG("_Wboson.mass(): " << _Wboson.mass() <<  " GeV");

      double mTW = transverseMass(_lepton, _neutrino_candidate[selectedidx]);
      // MSG_DEBUG("mTW (candidate) = " << mTW << " GeV");

      return make_pair(_Wboson, mTW);
    }

    //=====================================================================
    // fill top quark
    //=====================================================================
    Particle fillTopQuark(Particle _Wboson, const Jet& _bjet) {

      FourMomentum _reco_topquark = _Wboson.momentum() + _bjet.momentum();
      Particle _topquark(((_Wboson.pdgId() > 0) ? 6 : -6), _reco_topquark);
      PrintFourMomentum(0, _topquark);
      MSG_DEBUG("Mass(_topquark): " << Mass(_topquark) <<  " GeV");
      MSG_DEBUG("_topquark.mass(): " << _topquark.mass() <<  " GeV");

      double mTtop = transverseMass(_Wboson.momentum(), _bjet.momentum());
      MSG_DEBUG("mT(top-quark) = " << mTtop << " GeV");

      // fill top-quark information
      FillFourMomentum(_topquark, _h_objectmap["PreSel_top"], 0, true);
      // _h_top.at(7)->fill(sqrt(2*_Wboson.momentum().pT()/GeV*_bjet_selected.pT()/GeV*(1-cos(_Wboson.momentum().phi()-_bjet_selected.phi()))), _weight);
      _h_objectmap["PreSel_top"].at(7)->fill(mTtop, _weight);

      return _topquark;
    }

    //=====================================================================
    // BookHistograms
    //=====================================================================
    void BookHistograms() {
      // mc weight
      _h_weight = bookHisto1D("weight", 20, -10.5, 9.5);

      // stable charged particles pT distribution
      _h_charged.push_back(bookHisto1D("charged_N", 30, 0, 30));
      _h_charged.push_back(bookHisto1D("charged_E", 80, 0, 450));
      _h_charged.push_back(bookHisto1D("charged_Et", 80, 0, 450));
      _h_charged.push_back(bookHisto1D("charged_pt", 80, 0, 450));
      _h_charged.push_back(bookHisto1D("charged_pz", 80, 0, 450));
      _h_charged.push_back(bookHisto1D("charged_eta", 40, -5.0, 5.0));
      _h_charged.push_back(bookHisto1D("charged_phi", 32, 0.0, twopi));

    


      // all photons
      _h_photons.push_back(bookHisto1D("photons_N", 60, 0, 600));
      _h_photons.push_back(bookHisto1D("photons_E", 50, 0, 350));
      _h_photons.push_back(bookHisto1D("photons_Et", 50, 0, 350));
      _h_photons.push_back(bookHisto1D("photons_pt", 50, 0, 350));
      _h_photons.push_back(bookHisto1D("photons_pz", 50, 0, 350));
      _h_photons.push_back(bookHisto1D("photons_eta", 40, -5.0, 5.0));
      _h_photons.push_back(bookHisto1D("photons_phi", 32, 0.0, twopi));

      // prompt photons
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_N", 7, 0, 7));
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_E", 50, 0, 350));
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_Et", 50, 0, 350));
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_pt", 50, 0, 350));
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_pz", 50, 0, 350));
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_eta", 40, -5.0, 5.0));
      _h_photons_prompt.push_back(bookHisto1D("photons_prompt_phi", 32, 0.0, twopi));

      // all electrons
      _h_electrons.push_back(bookHisto1D("electrons_N", 5, 0, 5));
      _h_electrons.push_back(bookHisto1D("electrons_E", 50, 0, 350));
      _h_electrons.push_back(bookHisto1D("electrons_Et", 50, 0, 350));
      _h_electrons.push_back(bookHisto1D("electrons_pt", 50, 0, 350));
      _h_electrons.push_back(bookHisto1D("electrons_pz", 50, 0, 350));
      _h_electrons.push_back(bookHisto1D("electrons_eta", 40, -5.0, 5.0));
      _h_electrons.push_back(bookHisto1D("electrons_phi", 32, 0.0, twopi));

      // prompt electrons
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_N", 5, 0, 5));
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_E", 50, 0, 350));
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_Et", 50, 0, 350));
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_pt", 50, 0, 350));
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_pz", 50, 0, 350));
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_eta", 40, -5.0, 5.0));
      _h_electrons_prompt.push_back(bookHisto1D("electrons_prompt_phi", 32, 0.0, twopi));

      // dressed electrons
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_N", 5, 0, 5));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_E", 50, 0, 350));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_Et", 50, 0, 350));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_pt", 50, 0, 350));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_pz", 50, 0, 350));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_eta", 40, -5.0, 5.0));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_phi", 32, 0.0, twopi));
      _h_electrons_dressed.push_back(bookHisto1D("electrons_dressed_charged", 2, 0.0, 2));

      // all muons
      _h_muons.push_back(bookHisto1D("muons_N", 5, 0, 5));
      _h_muons.push_back(bookHisto1D("muons_E", 50, 0, 350));
      _h_muons.push_back(bookHisto1D("muons_Et", 50, 0, 350));
      _h_muons.push_back(bookHisto1D("muons_pt", 50, 0, 350));
      _h_muons.push_back(bookHisto1D("muons_pz", 50, 0, 350));
      _h_muons.push_back(bookHisto1D("muons_eta", 40, -5.0, 5.0));
      _h_muons.push_back(bookHisto1D("muons_phi", 32, 0.0, twopi));

      // prompt muons
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_N", 5, 0, 5));
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_E", 50, 0, 350));
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_Et", 50, 0, 350));
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_pt", 50, 0, 350));
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_pz", 50, 0, 350));
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_eta", 40, -5.0, 5.0));
      _h_muons_prompt.push_back(bookHisto1D("muons_prompt_phi", 32, 0.0, twopi));

      // dressed muons
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_N", 5, 0, 5));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_E", 50, 0, 350));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_Et", 50, 0, 350));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_pt", 50, 0, 350));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_pz", 50, 0, 350));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_eta", 40, -5.0, 5.0));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_phi", 32, 0.0, twopi));
      _h_muons_dressed.push_back(bookHisto1D("muons_dressed_charged", 2, 0.0, 2));
      

      // dressing photons
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_N", 7, 0, 7));
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_E", 50, 0, 350));
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_Et", 50, 0, 350));
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_pt", 50, 0, 350));
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_pz", 50, 0, 350));
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_eta", 40, -5.0, 5.0));
      _h_photons_fordressing.push_back(bookHisto1D("photons_fordressing_phi", 32, 0.0, twopi));

      // taus
      _h_taus.push_back(bookHisto1D("taus_N", 5, 0, 5));
      _h_taus.push_back(bookHisto1D("taus_E", 50, 0, 350));
      _h_taus.push_back(bookHisto1D("taus_Et", 50, 0, 350));
      _h_taus.push_back(bookHisto1D("taus_pt", 50, 0, 350));
      _h_taus.push_back(bookHisto1D("taus_pz", 50, 0, 350));
      _h_taus.push_back(bookHisto1D("taus_eta", 40, -5.0, 5.0));
      _h_taus.push_back(bookHisto1D("taus_phi", 32, 0.0, twopi));

      // prompt neutrinos
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_N", 5, 0, 5));
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_E", 50, 0, 350));
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_Et", 50, 0, 350));
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_pt", 50, 0, 350));
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_pz", 50, 0, 350));
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_eta", 40, -5.0, 5.0));
      _h_neutrinos_prompt.push_back(bookHisto1D("neutrinos_prompt_phi", 32, 0.0, twopi));

      // neutrinos
      _h_neutrinos.push_back(bookHisto1D("neutrinos_N", 10, 0, 10));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_E", 50, 0, 350));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_Et", 50, 0, 350));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_pt", 50, 0, 350));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_pz", 50, 0, 350));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_eta", 40, -5.0, 5.0));
      _h_neutrinos.push_back(bookHisto1D("neutrinos_phi", 32, 0.0, twopi));

      // ME (from all neutrinos)
      _h_ME_all.push_back(bookHisto1D("ME_all", 50, 0, 250));
      _h_ME_all.push_back(bookHisto1D("ME_all_eta", 40, -5.0, 5.0));
      _h_ME_all.push_back(bookHisto1D("ME_all_phi", 32, 0.0, twopi));

      // MET (from all neutrinos)
      _h_MET_all.push_back(bookHisto1D("MET_all", 50, 0, 2500));
      _h_MET_all.push_back(bookHisto1D("MET_all_phi", 32, 0.0, twopi));

      // ME (from prompt neutrinos)
      _h_ME_prompt.push_back(bookHisto1D("ME_prompt", 50, 0, 250));
      _h_ME_prompt.push_back(bookHisto1D("ME_prompt_eta", 40, -5.0, 5.0));
      _h_ME_prompt.push_back(bookHisto1D("ME_prompt_phi", 32, 0.0, twopi));

      // MET (from prompt neutrinos)
      _h_MET_prompt.push_back(bookHisto1D("MET_prompt", 50, 0, 2500));
      _h_MET_prompt.push_back(bookHisto1D("MET_prompt_phi", 32, 0.0, twopi));

      // fill MET (from sum of all or prompt neutrinos) vs. pT (from sum of prompt neutrinos)
      _h_METalldivMEPT = bookHisto1D("METalldivMEPT", 30, 0, 3);

      // B-hadrons
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_N", 15, 0, 15));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_E", 50, 0, 350));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_Et", 50, 0, 350));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_pt", 50, 0, 350));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_pz", 50, 0, 350));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_eta", 16, -5.0, 5.0));
      _h_B_hadrons.push_back(bookHisto1D("B_hadrons_phi", 32, 0.0, twopi));


      // lepton dressed 
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_N", 7, 0, 7));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_E", 50, 0, 350));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_Et", 50, 0, 350));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_pt", 50, 0, 350));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_pz", 50, 0, 350));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_eta", 40, -5.0, 5.0));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_phi", 32, 0.0, twopi));
      _h_leptons_dressed.push_back(bookHisto1D("leptons_dressed_charged", 2, 0, 2));


      
      

      // jets

      _h_exclusive_leadingJet.push_back(bookHisto1D("jet_exclusive_leadingJet", 50, 0, 350));

      _h_leadingJet.push_back(bookHisto1D("jet_leadingJet1", 50, 0, 350));
      _h_leadingJet.push_back(bookHisto1D("jet_leadingJet2", 50, 0, 350));
      _h_DeltaPhiLepLeadingJet.push_back(bookHisto1D("DeltaPhiLepLeadingJet", 32, 0, twopi));
      _h_DeltaRLepLeadingJet.push_back(bookHisto1D("DeltaRLepLeadingJet", 32, 0, twopi));
      _h_DeltaRJetJet.push_back(bookHisto1D("DeltaRJetJet", 32, 0, twopi));

      // good jets
      _h_jets.push_back(bookHisto1D("jets_N", 20, 0, 20));
      _h_jets.push_back(bookHisto1D("jets_E", 50, 0, 350));
      _h_jets.push_back(bookHisto1D("jets_Et", 50, 0, 350));
      _h_jets.push_back(bookHisto1D("jets_pt", 50, 0, 350));
      _h_jets.push_back(bookHisto1D("jets_pz", 50, 0, 350));
      _h_jets.push_back(bookHisto1D("jets_eta", 40, -5.0, 5.0));
      _h_jets.push_back(bookHisto1D("jets_phi", 32, 0.0, twopi));

      // light jets
      _h_ljets.push_back(bookHisto1D("ljets_N", 7, 0, 7));
      _h_ljets.push_back(bookHisto1D("ljets_E", 50, 0, 350));
      _h_ljets.push_back(bookHisto1D("ljets_Et", 50, 0, 350));
      _h_ljets.push_back(bookHisto1D("ljets_pt", 50, 0, 350));
      _h_ljets.push_back(bookHisto1D("ljets_pz", 50, 0, 350));
      _h_ljets.push_back(bookHisto1D("ljets_eta", 40, -5.0, 5.0));
      _h_ljets.push_back(bookHisto1D("ljets_phi", 32, 0.0, twopi));
      
      // b-jets
      _h_bjets.push_back(bookHisto1D("bjets_N", 7, 0, 7));
      _h_bjets.push_back(bookHisto1D("bjets_E", 50, 0, 350));
      _h_bjets.push_back(bookHisto1D("bjets_Et", 50, 0, 350));
      _h_bjets.push_back(bookHisto1D("bjets_pt", 50, 0, 350));
      _h_bjets.push_back(bookHisto1D("bjets_pz", 50, 0, 350));
      _h_bjets.push_back(bookHisto1D("bjets_eta", 40, -5.0, 5.0));
      _h_bjets.push_back(bookHisto1D("bjets_phi", 32, 0.0, twopi));
      
      // all light-jets
      _h_all_ljets.push_back(bookHisto1D("all_ljets_N", 9, 0, 9));
      _h_all_ljets.push_back(bookHisto1D("all_ljets_E", 50, 0, 350));
      _h_all_ljets.push_back(bookHisto1D("all_ljets_Et", 50, 0, 350));
      _h_all_ljets.push_back(bookHisto1D("all_ljets_pt", 50, 0, 350));
      _h_all_ljets.push_back(bookHisto1D("all_ljets_pz", 50, 0, 350));
      _h_all_ljets.push_back(bookHisto1D("all_ljets_eta", 40, -5.0, 5.0));
      _h_all_ljets.push_back(bookHisto1D("all_ljets_phi", 32, 0.0, twopi));

      // all b-jets
      _h_all_bjets.push_back(bookHisto1D("all_bjets_N", 7, 0, 7));
      _h_all_bjets.push_back(bookHisto1D("all_bjets_E", 50, 0, 350));
      _h_all_bjets.push_back(bookHisto1D("all_bjets_Et", 50, 0, 350));
      _h_all_bjets.push_back(bookHisto1D("all_bjets_pt", 50, 0, 350));
      _h_all_bjets.push_back(bookHisto1D("all_bjets_pz", 50, 0, 350));
      _h_all_bjets.push_back(bookHisto1D("all_bjets_eta", 40, -5.0, 5.0));
      _h_all_bjets.push_back(bookHisto1D("all_bjets_phi", 32, 0.0, twopi));

     
      // fill maps for objects
      _h_objectmap["PreSel_electron"] = FillHistogramMap("PreSel_electron");
      _h_objectmap["PreSel_muon"] = FillHistogramMap("PreSel_muon");
      _h_objectmap["PreSel_ljet"] = FillHistogramMap("PreSel_ljet");
      _h_objectmap["PreSel_bjet"] = FillHistogramMap("PreSel_bjet");
      _h_objectmap["PreSel_jet"] = FillHistogramMap("PreSel_jet");
      _h_objectmap["PreSel_MET"] = FillHistogramMap("PreSel_MET");
      _h_objectmap["PreSel_MTW"] = FillHistogramMap("PreSel_MTW");
      _h_objectmap["PreSel_HT"] = FillHistogramMap("PreSel_HT");
      _h_objectmap["PreSel_W"] = FillHistogramMap("PreSel_W");
     

      // Fill monotop anas

      _h_objectmap["PreSel_leadingJet"]=FillHistogramMap("PreSel_leadingJet");
      _h_objectmap["PreSel_lepton"]=FillHistogramMap("PreSel_lepton");
      _h_objectmap["PreSel_MTWMET"] = FillHistogramMap("PreSel_MTWMET");
      _h_objectmap["PreSel_DeltaPhiLepLeadingJet_el"]= FillHistogramMap("PreSel_DeltaPhiLepLeadingJet_el");
      _h_objectmap["PreSel_DeltaPhiLepLeadingJet_mu"]= FillHistogramMap("PreSel_DeltaPhiLepLeadingJet_mu");
      _h_objectmap["PreSel_DeltaPhiLepLeadingJet"]= FillHistogramMap("PreSel_DeltaPhiLepLeadingJet");
      _h_objectmap["PreSel_DeltaRLepLeadingJet_el"]= FillHistogramMap("PreSel_DeltaRLepLeadingJet_el");
      _h_objectmap["PreSel_DeltaRLepLeadingJet_mu"]= FillHistogramMap("PreSel_DeltaRLepLeadingJet_mu");
      _h_objectmap["PreSel_DeltaRLepLeadingJet"]= FillHistogramMap("PreSel_DeltaRLepLeadingJet");
      _h_objectmap["PreSel_DeltaRLepBJet_el"]= FillHistogramMap("PreSel_DeltaRLepBJet_el");
      _h_objectmap["PreSel_DeltaRLepBJet_mu"]= FillHistogramMap("PreSel_DeltaRLepBJet_mu");
      _h_objectmap["PreSel_DeltaRLepBJet"]= FillHistogramMap("PreSel_DeltaRLepBJet");
      _h_objectmap["PreSel_DeltaPhiLepBJet_el"]= FillHistogramMap("PreSel_DeltaPhiLepBJet_el");
      _h_objectmap["PreSel_DeltaPhiLepBJet_mu"]= FillHistogramMap("PreSel_DeltaPhiLepBJet_mu");
      _h_objectmap["PreSel_DeltaPhiLepBJet"]= FillHistogramMap("PreSel_DeltaPhiLepBJet");
      _h_objectmap["PreSel_DeltaRJetJet"]= FillHistogramMap("PreSel_DeltaRJetJet");
      _h_objectmap["PreSel_DeltaRJetBJet"]= FillHistogramMap("PreSel_DeltaRJetBJet");
      //_h_objectmap["PreSel_GoodJets_N"]= FillHistogramMap("PreSel_GoodJets_N");
      //_h_presel_jets.push_back(bookHisto1D("PreSel_jets_N", 20, 0, 20));

      //_h_jets.push_back(bookHisto1D("jets_N", 20, 0, 20));
      

      // Histos of Signal Region
      
      _h_signalmap["Sel_MET"]=bookHisto1D("Sel_MET",50, 100, 2500);
      _h_signalmap["Sel_leadingJet_pt"]=bookHisto1D("Sel_leadingJet_pt",50, 0, 400);
      _h_signalmap["Sel_lepton_pt"]=bookHisto1D("Sel_lepton_pt",50, 0, 400);
      _h_signalmap["Sel_DeltaRLepLeadingJet"]=bookHisto1D("Sel_DeltaRLepLeadingJet",32,0 ,twopi );
      _h_signalmap["Sel_DeltaRLepBJet"]= bookHisto1D("Sel_DeltaRLepBJet",32,0 ,twopi );
      _h_signalmap["Sel_DeltaRJetJet"]=  bookHisto1D("Sel_DeltaRJetJet",32,0 ,twopi);
      _h_signalmap["Sel_MTWMET"] = bookHisto1D("Sel_MTWMET",50, 0, 1000);

  }
    
  private:

    int eventNumber = 0;
    int numberOfPreselectedEvents = 0;
    int numberOfSelectedEvents = 0;

   
    //=====================================================================
    // Fill histogram
    //=====================================================================
    vector<Histo1DPtr> FillHistogramMap(string histoName) {

      string histo = Utils::SplitString(histoName, "_", true)[1];

      // cout << histoName << endl;
      // cout << histo << endl;

      vector<Histo1DPtr> _h_vector;
      if (histo == "MET") {
	_h_vector.push_back(bookHisto1D(histoName, 50, 0, 1000));
	_h_vector.push_back(bookHisto1D(histoName+"_phi", 32, 0.0, twopi));
      }
      else if (histo =="MTWMET"){ _h_vector.push_back(bookHisto1D(histoName, 50, 0, 2500));}
      else if (histo =="leadingJet"){_h_vector.push_back(bookHisto1D(histoName+"_pt", 50, 0, 500));}
      else if (histo == "MTW") { _h_vector.push_back(bookHisto1D(histoName, 35, 50, 120)); }
      else if (histo == "HT") { _h_vector.push_back(bookHisto1D(histoName, 70, 0, 700)); }
      else if (histo == "DeltaPhiLepLeadingJet" or histo == "DeltaPhiLepLeadingJet_el" or histo == "DeltaPhiLepLeadingJet_mu"){ _h_vector.push_back(bookHisto1D(histoName,32, 0, twopi));}
      else if (histo == "DeltaRLepLeadingJet" or histo == "DeltaRLepLeadingJet_el" or histo == "DeltaRLepLeadingJet_mu"){ _h_vector.push_back(bookHisto1D(histoName,32, 0, twopi));}
      else if (histo == "DeltaPhiLepBJet" or histo == "DeltaPhiLepBJet_el" or histo == "DeltaPhiLepBJet_mu"){ _h_vector.push_back(bookHisto1D(histoName,32, 0, twopi));}
      else if (histo == "DeltaRLepBJet" or histo == "DeltaRLepBJet_el" or histo == "DeltaRLepBJet_mu"){ _h_vector.push_back(bookHisto1D(histoName,32, 0, twopi));}
      else if (histo == "DeltaRJetJet" or histo == "DeltaRJetBJet" ){ _h_vector.push_back(bookHisto1D(histoName,32, 0, twopi));}
      //else if (histo ==  "GoodJets") { _h_vector.push_back(bookHisto1D(histoName+"_N",4, 0, 4));}
      else if (histo == "W" || histo == "top") {
	_h_vector.push_back(bookHisto1D(histoName+"_E", 50, 0, 700));
	_h_vector.push_back(bookHisto1D(histoName+"_Et", 50, 0, 700));
	_h_vector.push_back(bookHisto1D(histoName+"_pt", 50, 0, 700));
	_h_vector.push_back(bookHisto1D(histoName+"_pz", 50, 0, 700));
	_h_vector.push_back(bookHisto1D(histoName+"_eta", 40, -5.0, 5.0));
	_h_vector.push_back(bookHisto1D(histoName+"_phi", 32, 0.0, twopi));
	//if (histo=="chargedLepton") {
	// _h_vector.push_back(bookHisto1D(histoName+"_N", 30, 0, 30));}
	if (histo == "W") {
	  _h_vector.push_back(bookHisto1D(histoName+"_m", 30, 65, 95));
	  _h_vector.push_back(bookHisto1D(histoName+"_mt", 30, 55, 85));
	}
	else {
	  _h_vector.push_back(bookHisto1D(histoName+"_m", 30, 50, 300));
	  _h_vector.push_back(bookHisto1D(histoName+"_mt", 30, 0, 350));
	}
	
      }
      else {
	_h_vector.push_back(bookHisto1D(histoName+"_E", 50, 0, 350));
	_h_vector.push_back(bookHisto1D(histoName+"_Et", 50, 0, 350));
	_h_vector.push_back(bookHisto1D(histoName+"_pt", 50, 0, 350));
	_h_vector.push_back(bookHisto1D(histoName+"_pz", 50, 0, 350));
	_h_vector.push_back(bookHisto1D(histoName+"_eta", 40, -5.0, 5.0));
	_h_vector.push_back(bookHisto1D(histoName+"_phi", 32, 0.0, twopi));
	
      }
      return _h_vector;
    }

    //=====================================================================
    // Read configuration file
    //=====================================================================
    void ReadConfigFile(std::string inputFile) {
      std::ifstream f;  
      string line;
      f.open(inputFile.c_str());
      if(!f.is_open()) {
	MSG_ERROR("Configuration file is missing in run folder!!! ---> EXIT.");
	exit(0);
      }
      
      string sub1;
      string sub2;
      
      while (!f.eof()) {
	getline(f, line);  
	if (Utils::FindSubstringFromString(line, "CrossSection")) { cross_section = Utils::ToFloat(Utils::SplitString(line, "=", true)[1]); }
      }
      f.close();
    }
    
    double cross_section;
    double _weight;
    
    /// @name Histograms
    //@{

    Histo1DPtr _h_weight;
    

    vector<Histo1DPtr> _h_charged;
    
  

    vector<Histo1DPtr> _h_photons;
    vector<Histo1DPtr> _h_photons_prompt;
    vector<Histo1DPtr> _h_photons_fordressing;

    vector<Histo1DPtr> _h_electrons;
    vector<Histo1DPtr> _h_electrons_prompt;
    vector<Histo1DPtr> _h_electrons_dressed;

    vector<Histo1DPtr> _h_muons;
    vector<Histo1DPtr> _h_muons_prompt;
    vector<Histo1DPtr> _h_muons_dressed;

    vector<Histo1DPtr> _h_leptons_dressed;
    
    vector<Histo1DPtr> _h_taus;

    vector<Histo1DPtr> _h_neutrinos_prompt;
    vector<Histo1DPtr> _h_neutrinos;

    vector<Histo1DPtr> _h_ME_all;
    vector<Histo1DPtr> _h_MET_all;
    vector<Histo1DPtr> _h_ME_prompt;
    vector<Histo1DPtr> _h_MET_prompt;
    Histo1DPtr _h_METalldivMEPT;

    vector<Histo1DPtr> _h_B_hadrons;

    vector<Histo1DPtr> _h_jets;
    // vector<Histo1DPtr> _h_presel_jets;
    vector<Histo1DPtr> _h_exclusive_leadingJet;
    vector<Histo1DPtr> _h_leadingJet;
    vector<Histo1DPtr> _h_DeltaPhiLepLeadingJet;
    vector<Histo1DPtr> _h_DeltaRLepLeadingJet;
    vector<Histo1DPtr> _h_DeltaRJetJet;
    vector<Histo1DPtr> _h_ljets;
    vector<Histo1DPtr> _h_bjets;
    vector<Histo1DPtr> _h_all_ljets;
    vector<Histo1DPtr> _h_all_bjets;

   
    // (pre-)selected events
    map<string, vector<Histo1DPtr> > _h_objectmap;
    map<string, Histo1DPtr > _h_signalmap;
    
    //@}
    
   inline void FillFourMomentum(Particle _p, vector<Histo1DPtr> _histo, int index=1, bool fillMass=false) {
      FillFourMomentum(_p.momentum(), _histo, index, fillMass);
    }
    inline void FillFourMomentum(DressedLepton _p, vector<Histo1DPtr> _histo, int index=1, bool fillMass=false) {
      FillFourMomentum(_p.momentum(), _histo, index, fillMass);
    }
    inline void FillFourMomentum(const Jet& _j, vector<Histo1DPtr> _histo, int index=0, bool fillMass=false) {
      FillFourMomentum(_j.momentum(), _histo, index, fillMass);
    }

    // transverse mass
    inline double transverseMass(FourMomentum v1, FourMomentum v2) {
      // return sqrt( (v1.Et() + v2.Et())*(v1.Et() + v2.Et()) - (v1+v2).perp()*(v1+v2).perp() );
      // return sqrt(2*v1.pT()/GeV*v2.pT()/GeV*(1-cos(v1.phi()-v2.phi())));
      return sqrt(2*v1.pT()/GeV*v2.pT()/GeV*(1-cos(deltaPhi(v1, v2))));
    }
    inline double transverseMass(FourMomentum v1) {
      // return sqrt(pow(v1.Et(), 2) - pow(v1.px(), 2) - pow(v1.py(), 2));
      return sqrt(pow(v1.Et()/GeV, 2) - pow(v1.pz()/GeV, 2));
    }
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_monotop_Truth);
}

