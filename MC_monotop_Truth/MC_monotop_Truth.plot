# BEGIN PLOT /MC_monotop_Truth/*
#NormalizeToIntegral=1
#NormalizeToSum=1
LegendXPos=0.60
LegendYPos=0.95
RatioPlotYMin=0.75
RatioPlotYMax=1.25
YLabel=Fraction of events
FullRange=0
ShowZero=1
XTwosidedTicks=1
YTwosidedTicks=1
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/weight
Title=MC generator weight
XLabel=weight
LogY=0
#XMin=-5.0
#XMax=+5.0
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/charged_N
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=charged particle multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/charged_E
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/charged_Et
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/charged_pt
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/charged_pz
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/charged_eta
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
#XMin=-4.0
#XMax=+4.0
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/charged_phi
Title=charged particles ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=#phi [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

#====================================================

# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_N
Title=leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=charged leptons multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_E
Title=leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_Et
Title=charged leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_pt
Title=charged leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_pz
Title=charged leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_eta
Title=charged leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
#XMin=-5.0
#XMax=+5.0
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT




# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_phi
Title=charged leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=#phi [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/leptons_dressed_charged
Title=charged leptons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=charged ([0:1] ${-}$, [1:2] ${+}$)
BinXLabel(1,"{+}")
BinXLabel(2,"{-}")
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT



##===============================================================




# BEGIN PLOT /MC_monotop_Truth/photons_N
Title=multiplicity of photons ($|\eta|<$4.5)
XLabel=photon multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_E
Title=photons ($|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_Et
Title=photons ($|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_pt
Title=photons ($|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_pz
Title=photons ($|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_eta
Title=photons ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_phi
Title=photons ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_N
Title=multiplicity of prompt photons ($|\eta|<$4.5)
XLabel=photon multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_E
Title=prompt photons ($|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_Et
Title=prompt photons ($|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_pt
Title=prompt photons ($|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_pz
Title=prompt photons ($|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_eta
Title=prompt photons ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_prompt_phi
Title=prompt photons ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_N
Title=multiplicity of dressing photons ($|\eta|<$4.5)
XLabel=photon multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_E
Title=dressing photons ($|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_Et
Title=dressing photons ($|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_pt
Title=dressing photons ($|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_pz
Title=dressing photons ($|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_eta
Title=dressing photons ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/photons_fordressing_phi
Title=dressing photons ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
# END PLOT

#=====================================================================

# BEGIN PLOT /MC_monotop_Truth/electrons_N
Title=multiplicity of all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=electron multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_E
Title=all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_Et
Title=all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_pt
Title=all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_pz
Title=all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_eta
Title=all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.5
XMax=+2.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_phi
Title=all electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_N
Title=multiplicity of prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=electron multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_E
Title=prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_Et
Title=prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_pt
Title=prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_pz
Title=prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_eta
Title=prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_prompt_phi
Title=prompt electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_N
Title=multiplicity of dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=electron multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_E
Title=dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_Et
Title=dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_pt
Title=dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_pz
Title=dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_eta
Title=dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_phi
Title=dressed electrons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/electrons_dressed_charged
Title=charged electrons dressed ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=charged ([0:1] ${-}$, [1:2] ${+}$)
XBinLabel(1,"e^{-}")
XBinLabel(2,"e^{+}")
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT



## ==========================================

# BEGIN PLOT /MC_monotop_Truth/muons_N
Title=multiplicity of all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=muon multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_E
Title=all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_Et
Title=all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_pt
Title=all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_pz
Title=all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_eta
Title=all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_phi
Title=all muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_N
Title=multiplicity of prompt muons ($p_T>30 GeV, $|\eta|<$2.5)
XLabel=muon multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_E
Title=prompt muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_Et
Title=prompt muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_pt
Title=prompt muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_pz
Title=prompt muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_eta
Title=prompt muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_prompt_phi
Title=prompt muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_N
Title=multiplicity of dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=muon multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_E
Title=dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_Et
Title=dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_pt
Title=dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_pz
Title=dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_eta
Title=dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/muons_dressed_phi
Title=dressed muons ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/muons_dressed_charged
Title=dressed muons charged($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=charged ([0:1] ${-}$, [1:2] ${+}$)
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT



## ==========================================

# BEGIN PLOT /MC_monotop_Truth/taus_N
Title=multiplicity of taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=tau multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/taus_E
Title=taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/taus_Et
Title=taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/taus_pt
Title=taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/taus_pz
Title=taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/taus_phi
Title=taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/taus_eta
Title=taus ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.5
XMax=+2.5
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/neutrinos_N
Title=multiplicity of all neutrinos ($|\eta|<$4.5)
XLabel=neutrino multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_E
Title=all neutrinos ($|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_Et
Title=all neutrinos ($|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_pt
Title=all neutrinos ($|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_pz
Title=all neutrinos ($|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_eta
Title=all neutrinos ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_phi
Title=all neutrinos ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_N
Title=multiplicity of prompt neutrinos ($|\eta|<$4.5)
XLabel=neutrino multiplicity
LogY=1
#XMin=0
#XMax=10
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_E
Title=prompt neutrinos ($|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_Et
Title=prompt neutrinos ($|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_pt
Title=prompt neutrinos ($|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_pz
Title=prompt neutrinos ($|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_eta
Title=prompt neutrinos ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/neutrinos_prompt_phi
Title=prompt neutrinos ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/ME_all
Title=$E_T$ all neutrinos and DM ($|\eta|<$4.5)
XLabel=$E_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ME_all_phi
Title=$E_T$ all neutrinos and DM ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=150
LegendXPos=0.35
LegendYPos=0.40
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ME_all_eta
Title=$E_T$ all neutrinos and DM ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.52
XMax=+4.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/ME_prompt
Title=$E_T$ sum of prompt neutrinos and DM ($|\eta|<$4.5)
XLabel=$E_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ME_prompt_phi
Title=$E_T$  sum of prompt neutrinos and DM ($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=150
LegendXPos=0.35
LegendYPos=0.40
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ME_prompt_eta
Title=$E_T$ sum of prompt neutrinos and DM ($|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.52
XMax=+4.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/MET_all
Title=$E_T^{\text{miss}}$ sum of all neutrinos and DM ($|\eta|<$4.5)
XLabel=$E_T^{\text{miss}}$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/MET_all_phi
Title=$E_T^{\text{miss}}$ sum of all neutrinos and DM($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=150
LegendXPos=0.35
LegendYPos=0.40
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/MET_prompt
Title=$E_T^{\text{miss}}$ sum of prompt neutrinos and DM ($|\eta|<$4.5)
XLabel=$E_T^{\text{miss}}$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/MET_prompt_phi
Title=$E_T^{\text{miss}}$ sum of prompt neutrinos and DM($|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=150
LegendXPos=0.35
LegendYPos=0.40
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/METalldivMEPT
Title=$E_T^{\text{miss}}$ from  sum of all neutrinos / $p_T$ from prompt neutrinos ($|\eta|<$4.5)
XLabel=$E_T^{\text{miss}} / p_T$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_N
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=$B$ hadron multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_E
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_Et
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_eta
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
#XMin=-4.0
#XMax=+4.0
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_phi
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=#phi [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_pt
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/B_hadrons_pz
Title=$B$ hadrons ($p_T>$5 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/DeltaPhiLepLeadingJet
Title= DeltaPhi(lep, j) $p_T^>$30, $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5	
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/DeltaRLepLeadingJet
Title= DeltaR(lep, j) $p_T^>$30, $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5
XLabel=$\DeltaR$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/DeltaRJetJet
Title= DeltaR(j1, j2) $p_T^>$30, $|\eta^{jet}|<$4.5 
XLabel=$\DeltaR$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

## ==========================================


# BEGIN PLOT /MC_monotop_Truth/jet_exclusive_leadingJet
Title=1 jet exclusive ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/jet_leadingJet1
Title=leading jet 1 ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/jet_leadingJet2
Title=leading jet 2  ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT



## ==========================================


# BEGIN PLOT /MC_monotop_Truth/jets_N
Title=multiplicity of jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=jet multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/jets_E
Title=jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/jets_Et
Title=jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/jets_pt
Title=jets ($p_T>$ 30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/jets_pz
Title=jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/jets_eta
Title=jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/jets_phi
Title=jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/ljets_N
Title=multiplicity of light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=jet multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ljets_E
Title=light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ljets_Et
Title=light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ljets_pt
Title=light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ljets_pz
Title=light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ljets_eta
Title=light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/ljets_phi
Title=light-jets ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/bjets_N
Title=multiplicity of b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=b-jet multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/bjets_E
Title=b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/bjets_Et
Title=b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/bjets_pt
Title=b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/bjets_pz
Title=b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/bjets_eta
Title=b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/bjets_phi
Title=b-jets ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/all_ljets_N
Title=multiplicity of all light-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=b-jet multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_ljets_E
Title=light-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_ljets_Et
Title=light-jets ($|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_ljets_pt
Title=light-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_ljets_pz
Title=light-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_ljets_eta
Title=light-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_ljets_phi
Title=light-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/all_bjets_N
Title=multiplicity of all b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=b-jet multiplicity
LogY=0
#XMin=0
#XMax=10
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_bjets_E
Title=b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_bjets_Et
Title=b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_bjets_pt
Title=b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_bjets_pz
Title=b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_bjets_eta
Title=b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/all_bjets_phi
Title=b-jets ($p_T>$7 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================



# BEGIN PLOT /MC_monotop_Truth/PreSel_GoodJets_N
Title=PreSel :: Good Jets $p_T>$30 GeV, $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5
XLabel=$Multiplicity Good Jets$ [rad]
LogY=1
XMin=0
XMax=4
LegendXPos=0.75
LegendYPos=0.80
# END PLOT




# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaPhiLepLeadingJet
Title=PreSel :: DeltaPhi(lep, leadj) (el or mu ($p_T>$30 GeV, $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaPhiLepLeadingJet_el
Title=PreSel :: DeltaPhi(e, leadj)($p_T>$30 GeV, $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaPhiLepLeadingJet_mu
Title=PreSel :: DeltaPhi(mu, leadj)($p_T>$30 GeV, $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5) 
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT



## ==========================================


# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRLepLeadingJet
Title=PreSel :: DeltaR(lep, leadj) (el or mu ($p_T>$30 GeV) $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)  
XLabel=$\Delta R$ 
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRLepLeadingJet_el
Title=PreSel :: DeltaR(e, leadj) ( el $p_T>$30 GeV  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)  
XLabel=$\Delta$ R
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRLepLeadingJet_mu
Title=PreSel :: DeltaR(mu, leadj) ( mu $p_T>$30 GeV  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\Delta$ R[rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# ==========================================
# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaPhiLepBJet
Title=PreSel :: DeltaPhi(lep, bj) (el or mu $p_T>$30 GeV,  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaPhiLepBJet_el
Title=PreSel :: DeltaPhi(e, bj) ( el $p_T>$30 GeV,  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaPhiLepBJet_mu
Title=PreSel :: DeltaPhi(mu,bj) ( mu  $p_T>$30 GeV,  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\Delta\phi$ [rad]
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT



## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRLepBJet
Title=PreSel :: DeltaR(lep, bj) (el or mu $p_T>$30 GeV,  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\Delta R$
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRLepBJet_el
Title=PreSel :: DeltaR(e, bj) ( el  $p_T>$30 GeV,  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\DeltaR$
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRLepBJet_mu
Title=PreSel :: DeltaR(mu,bj) ( mu $p_T>$30 GeV,  $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)   
XLabel=$\DeltaR$ [rad]   
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_DeltaRJetJet
Title=PreSel :: DeltaR(jet0, jet1) ($p_T^{jet}>$30 GeV, $|\eta|<$2.5)
XLabel=$\DeltaR$ 
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT



## ==========================================



# BEGIN PLOT /MC_monotop_Truth/PreSel_electron_E
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.47)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_electron_Et
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_electron_pt
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_electron_pz
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_electron_eta
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_electron_phi
Title=PreSel :: electron ($p_T>$25 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_muon_E
Title=PreSel :: muon ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_muon_Et
Title=PreSel :: muon ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_muon_pt
Title=PreSel :: muon ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_muon_pz
Title=PreSel :: muon ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_muon_eta
Title=PreSel :: muon ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_muon_phi
Title=PreSel :: muon ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_ljet_E
Title=PreSel :: light-jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_ljet_Et
Title=PreSel :: light-jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_ljet_pt
Title=PreSel :: light-jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_ljet_pz
Title=PreSel :: light-jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_ljet_eta
Title=PreSel :: light-jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=$\eta$
LogY=0
XMin=-4.5
XMax=+4.5
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_ljet_phi
Title=PreSel :: light-jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_bjet_E
Title=PreSel :: b-jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_bjet_Et
Title=PreSel :: b-jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_bjet_pt
Title=PreSel :: b-jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END 
PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_bjet_pz
Title=PreSel :: b-jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_bjet_eta
Title=PreSel :: b-jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.5
XMax=+2.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_bjet_phi
Title=PreSel :: b-jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

## ==========================================
# BEGIN PLOT /MC_monotop_Truth/PreSel_leadingJet_pt
Title=PreSel :: jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT




# BEGIN PLOT /MC_monotop_Truth/PreSel_MTW
Title=PreSel :: $m_T$ of the lepton-$E_T^{\text{miss}}$ system ($m_T(\ell, E_T^{\text{miss}})>$60 GeV, $|\eta|<$2.5)
XLabel=$m_T(\ell, E_T^{\text{miss}})$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_MET
Title=PreSel :: $E_T^{\text{miss}}$ from the sum of prompt neutrinos ($E_T^{\text{miss}}>$60 GeV, $|\eta|<$4.5)
XLabel=$E_T^{\text{miss}}$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_MET_phi
Title=PreSel :: $E_T^{\text{miss}}$ from the sum of prompt neutrinos ($E_T^{\text{miss}}>$60 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=150
LegendXPos=0.35
LegendYPos=0.40
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_MTWMET
Title=PreSel :: $E_T^{\text{miss}}>$60 GeV, $|\eta|<$2.5  (mT(lepton, MET) + MET)
XLabel=$E_T^{\text{miss}}$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT
## ==========================================




# BEGIN PLOT /MC_monotop_Truth/PreSel_HT
Title=PreSel :: Sum of the $p_T$ of all final objects ($H_T>$195 GeV, $|\eta|<$4.5)
XLabel= $H_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_E
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=E [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_Et
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=E$_T$ [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_pt
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/PreSel_W_pz
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=p$_Z$ [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_phi
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_eta
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=$\eta$
#LogY=0
#XMin=-4.5
#XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_m
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=m [GeV]
#LogY=0
#XMin=0
#XMax=150
Rebin=2
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_W_mt
Title=PreSel :: reconstructed W boson ($m_T(W)>$60 GeV, $|\eta|<$4.5)
XLabel=m$_T$ [GeV]
LogY=0
#XMin=0
#XMax=150
LegendXPos=0.10
LegendYPos=0.90
Rebin=1
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_E
Title=PreSel :: top quark
XLabel=E [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_Et
Title=PreSel :: top quark
XLabel=E$_T$ [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_pt
Title=PreSel :: top quark
XLabel=p$_T$ [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/PreSel_top_pz
Title=PreSel :: top quark
XLabel=p$_Z$ [GeV]
#LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_phi
Title=PreSel :: top quark
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_eta
Title=PreSel :: top quark
XLabel=$\eta$
#LogY=0
#XMin=-4.5
#XMax=+4.5
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_m
Title=PreSel :: top quark
XLabel=m [GeV]
LogY=0
#XMin=100
#XMax=210
Rebin=1
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_top_mt
Title=PreSel :: top quark
XLabel=m$_T$ [GeV]
#LogY=0
#XMin=0
#XMax=350
#Rebin=0
# END PLOT

## ==========================================

# BEGIN PLOT /MC_monotop_Truth/PreSel_lepton_E
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.47)
XLabel=E [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_lepton_Et
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=E$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_lepton_pt
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_lepton_pz
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_Z$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_lepton_eta
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\eta$
LogY=0
XMin=-2.52
XMax=+2.52
#Rebin=0
LegendXPos=0.35
LegendYPos=0.40
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/PreSel_lepton_phi
Title=PreSel :: electron ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=$\phi$ [rad]
LogY=0
#XMin=0
#XMax=6.28
#Rebin=0
#LegendXPos=0.35
#LegendYPos=0.40
# END PLOT


## ========================================================





# BEGIN PLOT /MC_monotop_Truth/Sel_MET
Title=Sel :: $E_T^{\text{miss}}$ from the sum of prompt neutrinos ($E_T^{\text{miss}}>$120 GeV, $|\eta|<$4.5)
XLabel=$E_T^{\text{miss}}$ [GeV]
LogY=0
#XMin=0
#XMax=150
#Rebin=0
# END PLOT

# BEGIN PLOT /MC_monotop_Truth/Sel_leadingJet_pt
Title=Sel :: jet ($p_T>$30 GeV, $|\eta|<$4.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/Sel_lepton_pt
Title=PreSel :: jet ($p_T>$30 GeV, $|\eta|<$2.5)
XLabel=p$_T$ [GeV]
LogY=1
#XMin=0
#XMax=150
#Rebin=0
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/Sel_DeltaRLepLeadingJet
Title=Sel :: DeltaR(lep, leadj) (el or mu ($p_T>$30 GeV) $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)  
XLabel=$\Delta R$ 
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/Sel_DeltaRLepBJet
Title=Sel :: DeltaR(lep, bj) (el or mu ($p_T>$30 GeV) $|\eta^{jet}|<$4.5 and $|\eta^{lep}|<$2.5)  
XLabel=$\Delta R$ 
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT


# BEGIN PLOT /MC_monotop_Truth/Sel_DeltaRJetJet
Title=Sel :: DeltaR(j, j) ($p_T>$30 GeV) $|\eta^{jet}|<$4.5)  
XLabel=$\Delta R$ 
LogY=0
#XMin=-6.28
#XMax=6.28
#Rebin=0
LegendXPos=0.75
LegendYPos=0.80
# END PLOT



# BEGIN PLOT /MC_monotop_Truth/Sel_MTWMET
Title=Sel :: $E_T^{\text{miss}}>$120 GeV, $|\eta|<$2.5) + mT(lepton, MET) + MET
XLabel=$E_T^{\text{miss}}$ [GeV]
LogY=0
XMin=0
#XMax=150
#Rebin=0
# END PLOT
## ==========================================

