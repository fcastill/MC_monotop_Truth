# Project: Rivet - MC_monotop_Truth

MC validation of the non-resonant monotop (w -> lnu) production in pp collisions with the ATLAS detector.

## How to compile this rivet routine on lxplus

Use the following commands to compile the MC_monotop_Truth rivet routine:

```
setupATLAS --quiet
lsetup "asetup 20.20.8.1,here"
export RUCIO_ACCOUNT="$USER"
rivet-buildplugin RivetMC_monotop_Truth.so MC_monotop_Truth.cc
```

if ```setupATLAS``` is not working try:

```
export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh  --quietlsetup "asetup 20.20.8.1,here"
export RUCIO_ACCOUNT="$USER"
rivet-buildplugin RivetMC_sch_Truth.so MC_monotop_Truth.cc
```
